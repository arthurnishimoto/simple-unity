﻿Shader "Custom/SIMPLE_MultiColorCube" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
		_Color("Color", Color) = (1,1,1,1)
		_Color2("Color2", Color) = (1,1,1,1)
		_PointSize("Point Size", float) = 1.0
		unif_FieldMax("Field Max", float) = 1.0
		unif_FieldMin("Field Min", float) = 0.0
		unif_W1("W1", float) = 0.0
		unif_W2("W2", float) = 0.0
		unif_W3("W3", float) = 0.0
		unif_W4("W4", float) = 0.0
	}
	SubShader{
		Pass{
			Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
			LOD 200
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			# define UNITY_SHADER_NO_UPGRADE 1
			#pragma vertex vert
			#pragma fragment frag
			#pragma geometry geom

			#include "UnityCG.cginc"
			#include "Lighting.cginc"

			# define VERT_COUNT 36
			float unif_FieldMax;
			float unif_FieldMin;
			float unif_W1;
			float unif_W2;
			float unif_W3;
			float unif_W4;
			float4 _Color;
			float4 _Color2;
			float _PointSize;

			float4 _MainTex_ST;
			sampler2D _MainTex;
			float3 vertex_light_position;

			struct VertexInput {
				float4 pos : POSITION;
				float4 col : COLOR;
			};

			struct VertexOutput {
				float4 pos : SV_POSITION;
				float4 col : COLOR0;
			};

			struct GeometryOut {
				float4 pos : SV_POSITION;
				float2 uv_MainTex : TEXCOORD0;
				float4 col : COLOR0;
				float3 nor : NORMAL;
			};

			VertexOutput vert(VertexInput input) {
				VertexOutput o;

				o.col = input.col;

				// Passing on center vertex (tile to be built by geometry shader from it later)
				o.pos = input.pos;

				return o;
			}

			half4 frag(GeometryOut output) : COLOR{

				float x = output.uv_MainTex.x;
				float y = output.uv_MainTex.y;
				float zz = 1.0 - x*x - y*y;

				//if (zz <= 0.0)
				//	discard;

				float z = sqrt(zz);

				float3 normal = float3(x, y, z);

				float4 col;

				float3 w = float3(unif_W1, unif_W2, unif_W3);
				float3 inputData = float3(output.col.r, output.col.g, output.col.z);
				float colorW = (dot(w, inputData) - unif_FieldMin) / (unif_FieldMax - unif_FieldMin);

				float timeRatio = (inputData.r - unif_FieldMin) / (unif_FieldMax - unif_FieldMin);

				float3 fwdStartColor = float3(0.1, 0.7, 1.0); // Cyan/Fwd vector
				float3 upStartColor = float3(0.1, 1.0, 0.1); // Green/Up vector

				float3 fwdEndColor = float3(0.1, 0.2, 0.5);
				float3 upEndColor = float3(0.1, 0.5, 0.1);

				col.rgb = _Color.rgb;
				// o.col.rgb = mix(o.col.rgb, gl_FrontMaterial.diffuse.rgb, unif_W4);
				col.a = _Color.a;

				if (unif_W3 == 0)
					col.rgb = _Color.rgb;
				else if (unif_W3 == 1) // Color by type
				{
					if (inputData.g == 0) // Forward
						col.rgb = float3(0.1, 1.0, 1.0);
					else // Upper
						col.rgb = float3(0.1, 1.0, 0.1);
				}

				if (unif_W2 == 1) // Conductivity (CTD1) or Pressure (CTD2)
				{
					float value = 1 - (output.col.r - unif_FieldMin) / (unif_FieldMax - unif_FieldMin);
					col.rgb = lerp(_Color2.rgb, _Color.rgb, value);

					if (value > 1 || value < 0)
					{
						col.a = 0.0;
						discard;
					}
				}
				if (unif_W2 == 2) // Salinity (CTD1) or O2 Concentration (CTD2)
				{
					float value = 1 - (output.col.g - unif_FieldMin) / (unif_FieldMax - unif_FieldMin);
					col.rgb = lerp(_Color2.rgb, _Color.rgb, value);

					if (value > 1 || value < 0)
					{
						col.a = 0.0;
						discard;
					}
				}
				if (unif_W2 == 3) // Temperature (CTD1) or O2 Percent Saturation (CTD2)
				{
					float value = 1 - (output.col.b - unif_FieldMin) / (unif_FieldMax - unif_FieldMin);
					col.rgb = lerp(_Color2.rgb, _Color.rgb, value);

					if (value > 1 || value < 0)
					{
						col.a = 0.0;
						discard;
					}
				}

				if (unif_W4 == 1) // Color by timestamp
				{
					if (unif_W3 == 1)
					{
						if (inputData.g == 0) // Forward
							col.rgb = lerp(fwdStartColor, fwdEndColor, 1 - timeRatio);
						else // Upper
							col.rgb = lerp(upStartColor, upEndColor, 1 - timeRatio);
					}
					else if (unif_W3 == 0)
					{
						col.rgb = lerp(_Color.rgb, float3(0.1, 0.1, 0.1), 1 - timeRatio);
					}

					// Quick fix: Hide CTD if color by time (need more data to shader to color this correctly)
					if (unif_W2 != 0)
					{
						discard;
					}

					if (inputData.x <= unif_FieldMax)
						col.a = _Color.a;
					else
					{
						col.a = 0.0;
						discard;
					}
				}
				

				float3 lightDirection;
				float attenuation;
				if (unif_W1 == 0) // Use Lighting
				{
					// Lighting
					// https://en.wikibooks.org/wiki/Cg_Programming/Unity/Lighting_Textured_Surfaces
					if (0.0 == _WorldSpaceLightPos0.w) // directional light?
					{
						attenuation = 1.0; // no attenuation
						lightDirection = normalize(_WorldSpaceLightPos0.xyz);// -_WorldSpaceCameraPos.xyz);
					}
					float diffuse_value = max(dot(normal, vertex_light_position), 0.0);

					float3 ambientLighting =
						UNITY_LIGHTMODEL_AMBIENT.rgb * col.rgb;

					float3 diffuseReflection =
						attenuation * _LightColor0.rgb * col.rgb
						* max(0.0, dot(output.nor, lightDirection));

					col.rgb = col.rgb * ambientLighting + diffuseReflection;
				}
				return col;
				
			}

			// https://takinginitiative.wordpress.com/2011/01/12/directx10-tutorial-9-the-geometry-shader/
			[maxvertexcount(VERT_COUNT)]
			void geom(point VertexOutput vert[1], inout TriangleStream<GeometryOut> triStream) {
				
				vertex_light_position = normalize(_WorldSpaceLightPos0.xyz - _WorldSpaceCameraPos.xyz);

				// Based on:
				// https://forum.unity3d.com/threads/simple-cube-shader.313644/
				float f = _PointSize / 2.0f; // _PointSize = 1 -> one meter

				const float4 vc[VERT_COUNT] = {
					float4(-f,  f,  f, 0.0f), float4(f,  f,  f, 0.0f), float4(f,  f, -f, 0.0f), // Top                                 
					float4(-f,  f,  f, 0.0f), float4(f,  f, -f, 0.0f), float4(-f,  f, -f, 0.0f),

					float4(f,  f,  f, 0.00), float4(f,  -f,  f, 0.00), float4(f, -f, -f, 0.00), // Front
					float4(f,  f,  f, 0.00), float4(f, -f, -f, 0.00), float4(f,  f, -f, 0.00),

					float4(-f,  f,  f, 0.00), float4(-f, -f,  f, 0.00), float4(f, -f,  f, 0.00), // Left
					float4(-f,  f,  f, 0.00), float4(f,  -f,  f, 0.00), float4(f,  f,  f, 0.00),

					float4(f,  f, -f, 0.00), float4(f, -f, -f, 0.00), float4(-f, -f, -f, 0.00), // Right                                  
					float4(f,  f, -f, 0.00), float4(-f, -f, -f, 0.00), float4(-f,  f, -f, 0.00),

					float4(-f,  f, -f, 0.00), float4(-f, -f, -f, 0.00), float4(-f, -f,  f, 0.00), // Back
					float4(-f,  f, -f, 0.00), float4(-f, -f,  f, 0.00), float4(-f,  f,  f, 0.00),

					float4(f, -f,  f, 0.00), float4(-f, -f,  f, 0.00), float4(-f, -f, -f, 0.00),
					float4(f, -f,  f, 0.00), float4(-f, -f, -f, 0.00), float4(f, -f, -f, 0.00)
				};


				const float2 UV1[VERT_COUNT] = {
					float2(1.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),
					float2(1.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),

					float2(1.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),
					float2(1.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),

					float2(0.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),
					float2(1.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),

					float2(0.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),
					float2(1.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),

					float2(0.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),
					float2(1.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),

					float2(0.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f),
					float2(1.0f,    0.0f), float2(1.0f,    0.0f), float2(1.0f,    0.0f)
				};


				const float3 NOR[VERT_COUNT] = {
					
					float3(-1.0f,   0.0f,    0.0f), float3(-1.0f,   0.0f,    0.0f), float3(-1.0f,   0.0f,    0.0f), // Left
					float3(-1.0f,   0.0f,    0.0f), float3(-1.0f,   0.0f,    0.0f), float3(-1.0f,   0.0f,    0.0f),
					

					float3(0.0f,    0.0f,   -1.0f), float3(0.0f,    0.0f,   -1.0f), float3(0.0f,    0.0f,   -1.0f), // Back
					float3(0.0f,    0.0f,   -1.0f), float3(0.0f,    0.0f,   -1.0f), float3(0.0f,    0.0f,   -1.0f),

					float3(0.0f,   -1.0f,    0.0f), float3(0.0f,   -1.0f,    0.0f), float3(0.0f,   -1.0f,    0.0f), // Bottom
					float3(0.0f,   -1.0f,    0.0f), float3(0.0f,   -1.0f,    0.0f), float3(0.0f,   -1.0f,    0.0f),

					float3(0.0f,    1.0f,    0.0f), float3(0.0f,    1.0f,    0.0f), float3(0.0f,    1.0f,    0.0f), // Top
					float3(0.0f,    1.0f,    0.0f), float3(0.0f,    1.0f,    0.0f), float3(0.0f,    1.0f,    0.0f),

					float3(0.0f,    0.0f,   1.0f), float3(0.0f,    0.0f,   1.0f), float3(0.0f,    0.0f,   1.0f), // Front
					float3(0.0f,    0.0f,   1.0f), float3(0.0f,    0.0f,   1.0f), float3(0.0f,    0.0f,   1.0f),

					float3(1.0f,    0.0f,    0.0f), float3(1.0f,    0.0f,    0.0f), float3(1.0f,    0.0f,    0.0f), // Right
					float3(1.0f,    0.0f,    0.0f), float3(1.0f,    0.0f,    0.0f), float3(1.0f,    0.0f,    0.0f)

					/*
					float3(0.0f,    0.0f,    1.0f), float3(0.0f,    0.0f,    1.0f), float3(0.0f,    0.0f,    1.0f), // Left
					float3(0.0f,    0.0f,    1.0f), float3(0.0f,    0.0f,    1.0f), float3(0.0f,    0.0f,    1.0f),

					float3(-1.0f,   0.0f,    0.0f), float3(-1.0f,   0.0f,    0.0f), float3(-1.0f,   0.0f,    0.0f), // Back
					float3(-1.0f,   0.0f,    0.0f), float3(-1.0f,   0.0f,    0.0f), float3(-1.0f,   0.0f,    0.0f),

					float3(1.0f,    0.0f,    0.0f), float3(1.0f,    0.0f,    0.0f), float3(1.0f,    0.0f,    0.0f), // Front
					float3(1.0f,    0.0f,    0.0f), float3(1.0f,    0.0f,    0.0f), float3(1.0f,    0.0f,    0.0f),

					float3(0.0f,    0.0f,   -1.0f), float3(0.0f,    0.0f,   -1.0f), float3(0.0f,    0.0f,   -1.0f), // Right
					float3(0.0f,    0.0f,   -1.0f), float3(0.0f,    0.0f,   -1.0f), float3(0.0f,    0.0f,   -1.0f),

					float3(0.0f,    1.0f,    0.0f), float3(0.0f,    1.0f,    0.0f), float3(0.0f,    1.0f,    0.0f), // Top
					float3(0.0f,    1.0f,    0.0f), float3(0.0f,    1.0f,    0.0f), float3(0.0f,    1.0f,    0.0f)
					*/
				};

				const int TRI_STRIP[VERT_COUNT] = {
					0, 1, 2,  3, 4, 5,
					6, 7, 8,  9,10,11,
					12,13,14, 15,16,17,
					18,19,20, 21,22,23,
					24,25,26, 27,28,29,
					30,31,32, 33,34,35
				};

				GeometryOut v[VERT_COUNT];
				int i;

				// Assign new vertices positions 
				for (i = 0; i < VERT_COUNT; i++) {
					v[i].pos = vert[0].pos + vc[i];
					v[i].col = vert[0].col;
				}

				// Assign UV values
				for (i = 0; i < VERT_COUNT; i++) {
					// TRANSFORM_TEX "UnityCG.cginc"
					v[i].uv_MainTex = TRANSFORM_TEX(UV1[i], _MainTex);
				}

				// Assign Normal values
				for (i = 0; i < VERT_COUNT; i++) {
					v[i].nor = NOR[i];
				}

				// Position in VERT_COUNT space
				for (i = 0; i < VERT_COUNT; i++) {
					v[i].pos = mul(UNITY_MATRIX_MVP,v[i].pos);
				}

				// Build the cube tile by submitting triangle strip vertices
				for (i = 0; i < VERT_COUNT / 3; i++)
				{
					triStream.Append(v[TRI_STRIP[i * 3 + 0]]);
					triStream.Append(v[TRI_STRIP[i * 3 + 1]]);
					triStream.Append(v[TRI_STRIP[i * 3 + 2]]);

					triStream.RestartStrip();
				}
			}
			ENDCG
		}
	}
}
