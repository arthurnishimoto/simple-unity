﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/SIMPLE_MultiColorPoint" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_Color2("Color2", Color) = (1,1,1,1)
		unif_FieldMax("Field Max", float) = 1.0
		unif_FieldMin("Field Min", float) = 0.0
		unif_W1("W1", float) = 0.0
		unif_W2("W2", float) = 0.0
		unif_W3("W3", float) = 0.0
		unif_W4("W4", float) = 0.0
	}
		SubShader{
			Pass{
			LOD 200
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// #pragma geometry geom

			float unif_FieldMax;
			float unif_FieldMin;
			float unif_W1;
			float unif_W2;
			float unif_W3;
			float unif_W4;
			float4 _Color;
			float4 _Color2;

			struct VertexInput {
				float4 v : POSITION;
				float4 color: COLOR;
			};

			struct VertexOutput {
				float4 pos : SV_POSITION;
				float4 col : COLOR;
			};

			VertexOutput vert(VertexInput v) {
				VertexOutput o;
				o.pos = UnityObjectToClipPos(v.v);
				o.col = v.color;
				return o;
			}

			float4 frag(VertexOutput output) : COLOR{
				float3 w = float3(unif_W1, unif_W2, unif_W3);
				float3 inputData = float3(output.col.r, output.col.g, output.col.z);
				float colorW = (dot(w, inputData) - unif_FieldMin) / (unif_FieldMax - unif_FieldMin);

				float timeRatio = (inputData.r - unif_FieldMin) / (unif_FieldMax - unif_FieldMin);

				float3 fwdStartColor = float3(0.1, 0.7, 1.0); // Cyan/Fwd vector
				float3 upStartColor = float3(0.1, 1.0, 0.1); // Green/Up vector
				
				float3 fwdEndColor = float3(0.1, 0.2, 0.5);
				float3 upEndColor = float3(0.1, 0.5, 0.1);

				output.col.rgb = _Color.rgb;
				// o.col.rgb = mix(o.col.rgb, gl_FrontMaterial.diffuse.rgb, unif_W4);
				output.col.a = _Color.a;

				if (unif_W3 == 0)
					output.col.rgb = _Color.rgb;
				else if (unif_W3 == 1) // Color by type
				{
					if (inputData.g == 0) // Forward
						output.col.rgb = float3(0.1, 1.0, 1.0);
					else // Upper
						output.col.rgb = float3(0.1, 1.0, 0.1);
				}

				if (unif_W2 == 1) // Conductivity (CTD1) or Pressure (CTD2)
				{
					float value = 1 - (output.col.r - unif_FieldMin) / (unif_FieldMax - unif_FieldMin);
					output.col.rgb = lerp(_Color2.rgb, _Color.rgb, value);

					if (value > 1 || value < 0)
					{
						output.col.a = 0.0;
						discard;
					}
				}
				if (unif_W2 == 2) // Salinity (CTD1) or O2 Concentration (CTD2)
				{
					float value = 1 - (output.col.g - unif_FieldMin) / (unif_FieldMax - unif_FieldMin);
					output.col.rgb = lerp(_Color2.rgb, _Color.rgb, value);

					if (value > 1 || value < 0)
					{
						output.col.a = 0.0;
						discard;
					}
				}
				if (unif_W2 == 3) // Temperature (CTD1) or O2 Percent Saturation (CTD2)
				{
					float value = 1 - (output.col.b - unif_FieldMin) / (unif_FieldMax - unif_FieldMin);
					output.col.rgb = lerp(_Color2.rgb, _Color.rgb, value);

					if (value > 1 || value < 0)
					{
						output.col.a = 0.0;
						discard;
					}
				}

				if (unif_W4 == 1) // Color by timestamp
				{
					if (unif_W3 == 1)
					{
						if (inputData.g == 0) // Forward
							output.col.rgb = lerp(fwdStartColor, fwdEndColor, 1 - timeRatio);
						else // Upper
							output.col.rgb = lerp(upStartColor, upEndColor, 1 - timeRatio);
					}
					else if (unif_W3 == 0)
						output.col.rgb = lerp(_Color.rgb, float3(0.1, 0.1, 0.1), 1 - timeRatio);

					if (inputData.x <= unif_FieldMax)
						output.col.a = _Color.a;
					else
					{
						output.col.a = 0.0;
						discard;
					}
				}

				return output.col;
			}

			uniform float pointScale;

			float3 vertex_light_position;
			float4 eye_position;
			float sphere_radius;

			uniform float globalAlpha;

			// [maxvertexcount(3)]
			// void geom(point VertexOutput input[1], inout TriangleStream<VertexOutput> triStream) {
			// 
			// }
			ENDCG
		}
	}
	FallBack "Diffuse"
}
