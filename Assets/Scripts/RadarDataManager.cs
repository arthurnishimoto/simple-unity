﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadarDataManager : MonoBehaviour {

    LineRenderer[] renderers;

    bool toggle;

    [SerializeField]
    GameObject toggledIcon;

	// Use this for initialization
	void Start () {
        renderers = GetComponentsInChildren<LineRenderer>();
        toggle = !renderers[0].enabled;
        if(toggledIcon)
        {
            toggledIcon.SetActive(!toggle);
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ToggleRadar()
    {
        foreach(LineRenderer line in renderers)
        {
            line.enabled = toggle;
        }
        toggle = !toggle;

        if (toggledIcon)
        {
            toggledIcon.SetActive(!toggle);
        }
    }

    int scaleFactor;
    float scale;

    [SerializeField]
    UnityEngine.UI.Text scaleUIText;
    public void SetScale(float value)
    {
        scaleFactor = (int)value;
        UpdateSpeed();
    }

    void UpdateSpeed()
    {
        if (scaleFactor > 6)
            scaleFactor = 6;
        else if (scaleFactor < 0)
            scaleFactor = 0;

        switch (scaleFactor)
        {
            case (0): scale = 1; break;
            case (1): scale = 5; break;
            case (2): scale = 10; break;
            case (3): scale = 25; break;
            case (4): scale = 50; break;
            case (5): scale = 100; break;
            case (6): scale = 250; break;
        }

        if (scaleUIText)
            scaleUIText.text = (int)scale + "x";

        transform.localScale = new Vector3(1, scale, 1);
    }
}
