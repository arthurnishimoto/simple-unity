﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SIMPLEVehicleManager : MonoBehaviour {

    LineRenderer currentLine;
    float width = 0.1f;

    public Material stateLineMaterial;

    SIMPLEDataManager.SIMPLEData missionData;

    public static int statePositionIncrements = 4;

    public void GenerateStateLines(SIMPLEDataManager.SIMPLEData data)
    {
        string missionName = Path.GetFileName(data.GetFilePath()).Substring(0, 22) + "-artemis-state.stateData";

        GameObject g = new GameObject(missionName);
        g.transform.parent = GameObject.Find("World").transform;
        g.transform.localPosition = Vector3.zero;
        g.transform.localEulerAngles = Vector3.zero;
        g.transform.localScale = Vector3.one;

        currentLine = g.AddComponent<LineRenderer>();
        SetLineRendererDefaults(ref currentLine);

        Vector3[] positions = data.GetStatePositions(statePositionIncrements);
        currentLine.SetVertexCount(positions.Length);
        currentLine.SetPositions(positions);

        missionData = data;
    }

    public void UpdateStateLineTimestamp(long timestamp)
    {
        Vector3[] positions = missionData.GetStatePositions(4, timestamp);
        currentLine.SetVertexCount(positions.Length);
        currentLine.SetPositions(positions);
    }

    void SetLineRendererDefaults(ref LineRenderer lr)
    {
        currentLine.useWorldSpace = false;
        currentLine.receiveShadows = false;
        currentLine.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

        currentLine.material = stateLineMaterial;
        currentLine.SetWidth(width, width);
        currentLine.enabled = false;
    }
}
