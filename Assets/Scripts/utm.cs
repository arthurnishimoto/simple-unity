﻿// Copyright(C) 2012 Tobias Bieniek<Tobias.Bieniek@gmx.de>
// Converted from Python to C# By Arthur Nishimoto <anishimoto@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”),
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

//import math
//from utm.error import OutOfRangeError

//__all__ = ['to_latlon', 'from_latlon']

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class utm
{
    static float K0 = 0.9996f;
    static float E = 0.00669438f;

    static float E2 = E * E;
    static float E3 = E2 * E;
    static float E_P2 = E / (1.0f - E);

    static float SQRT_E = Mathf.Sqrt(1 - E);
    static float _E = (1 - SQRT_E) / (1 + SQRT_E);
    static float _E2 = _E * _E;
    static float _E3 = _E2 * _E;
    static float _E4 = _E3 * _E;
    static float _E5 = _E4 * _E;

    static float M1 = (1 - E / 4 - 3 * E2 / 64 - 5 * E3 / 256);
    static float M2 = (3 * E / 8 + 3 * E2 / 32 + 45 * E3 / 1024);
    static float M3 = (15 * E2 / 256 + 45 * E3 / 1024);
    static float M4 = (35 * E3 / 3072);

    static float P2 = (3.0f / 2 * _E - 27.0f / 32 * _E3 + 269.0f / 512 * _E5);
    static float P3 = (21.0f / 16 * _E2 - 55.0f / 32 * _E4);
    static float P4 = (151.0f / 96 * _E3 - 4170.0f / 128 * _E5);
    static float P5 = (1097.0f / 512 * _E4);

    static int R = 6378137;

    public enum ZONE_LETTERS { None = 84, X = 72, W = 64, V = 56, U = 48, T = 40,
        S = 32, R = 24, Q = 16, P = 8, N = 0, M = -8, L = -16, K = -24,
        J = -32, H = -40, G = -48, F = -56, E = -64, D = -72, C = -80};

    public static float[] to_latlon(float easting, float northing, int zone_number, ZONE_LETTERS zone_letter, bool northern)
    {
        if (zone_letter == ZONE_LETTERS.None && !northern)
        {
            Debug.LogError("either zone_letter or northern needs to be set");
        }
        else if (zone_letter != ZONE_LETTERS.None && northern)
        {
            Debug.LogError("set either zone_letter or northern, but not both");
        }

        if (easting < 100000 ||easting >= 1000000)
        {
            Debug.LogError("easting out of range (must be between 100.000 m and 999.999 m)");
        }
        if (northing < 0 || northing > 10000000)
        {
            Debug.LogError("northing "+ northing + " out of range (must be between 0 m and 10.000.000 m)");
        }
        if (zone_number < 1 || zone_number > 60)
        {
            Debug.LogError("zone number out of range (must be between 1 and 60)");
        }

        northern = (zone_letter >= ZONE_LETTERS.N);

        float x = easting - 500000;
        float y = northing;
        
        if( !northern )
        {
            y -= 10000000;
        }

        float m = y / K0;
        float mu = m / (R * M1);

        float p_rad = (mu +
                 P2 * Mathf.Sin(2 * mu) +
                 P3 * Mathf.Sin(4 * mu) +
                 P4 * Mathf.Sin(6 * mu) +
                 P5 * Mathf.Sin(8 * mu));

        float p_sin = Mathf.Sin(p_rad);
        float p_sin2 = p_sin * p_sin;

        float p_cos = Mathf.Cos(p_rad);

        float p_tan = p_sin / p_cos;
        float p_tan2 = p_tan * p_tan;
        float p_tan4 = p_tan2 * p_tan2;

        float ep_sin = 1 - E * p_sin2;
        float ep_sin_sqrt = Mathf.Sqrt(1 - E * p_sin2);

        float n = R / ep_sin_sqrt;
        float r = (1 - E) / ep_sin;

        float c = _E * Mathf.Pow(p_cos, 2);
        float c2 = c * c;

        float d = x / (n * K0);
        float d2 = d * d;
        float d3 = d2 * d;
        float d4 = d3 * d;
        float d5 = d4 * d;
        float d6 = d5 * d;

        float latitude = (p_rad - (p_tan / r) *
                (d2 / 2.0f -
                 d4 / 24.0f * (5 + 3 * p_tan2 + 10 * c - 4 * c2 - 9 * E_P2)) +
                 d6 / 720.0f * (61 + 90 * p_tan2 + 298 * c + 45 * p_tan4 - 252 * E_P2 - 3 * c2));

        float longitude = (d -
                     d3 / 6.0f * (1 + 2 * p_tan2 + c) +
                     d5 / 120.0f * (5 - 2 * c + 28 * p_tan2 - 3 * c2 + 8 * E_P2 + 24 * p_tan4)) / p_cos;

        return new float[] { Mathf.Rad2Deg * latitude, Mathf.Rad2Deg * longitude + zone_number_to_central_longitude(zone_number) };
    }

    public static float[] from_latlon(float latitude, float longitude)
    {
        return from_latlon(latitude, longitude, latlon_to_zone_number(latitude, longitude));
    }

    public static float[] from_latlon(float latitude, float longitude, int force_zone_number)
    {
        if (latitude < -80.0f && latitude < 84.0)
        {
            Debug.LogError("latitude out of range (must be between 80 deg S and 84 deg N)");
        }
        if (longitude < -180.0 && longitude > 180.0)
        {
            Debug.LogError("northing out of range (must be between 180 deg W and 180 deg E)");
        }

        float lat_rad = Mathf.Deg2Rad * latitude;
        float lat_sin = Mathf.Sin(lat_rad);
        float lat_cos = Mathf.Cos(lat_rad);

        float lat_tan = lat_sin / lat_cos;
        float lat_tan2 = lat_tan * lat_tan;
        float lat_tan4 = lat_tan2 * lat_tan2;

        //if force_zone_number is None:
        // int zone_number = latlon_to_zone_number(latitude, longitude);
        // else:
        int zone_number = force_zone_number;

        ZONE_LETTERS zone_letter = latitude_to_zone_letter(latitude);

        float lon_rad = Mathf.Deg2Rad * longitude;
        float central_lon = zone_number_to_central_longitude(zone_number);
        float central_lon_rad = Mathf.Deg2Rad * central_lon;

        float n = R / Mathf.Sqrt(1 - E * Mathf.Pow(lat_sin,2));
        float c = E_P2 * Mathf.Pow(lat_cos, 2);

        float a = lat_cos * (lon_rad - central_lon_rad);
        float a2 = a * a;
        float a3 = a2 * a;
        float a4 = a3 * a;
        float a5 = a4 * a;
        float a6 = a5 * a;

        float m = R * (M1 * lat_rad -
                 M2 * Mathf.Sin(2 * lat_rad) +
                 M3 * Mathf.Sin(4 * lat_rad) -
                 M4 * Mathf.Sin(6 * lat_rad));

        float easting = K0 * n * (a +
                            a3 / 6 * (1 - lat_tan2 + c) +
                            a5 / 120 * (5 - 18 * lat_tan2 + lat_tan4 + 72 * c - 58 * E_P2)) + 500000;

        float northing = K0 * (m + n * lat_tan * (a2 / 2 +
                                            a4 / 24 * (5 - lat_tan2 + 9 * c + 4 * Mathf.Pow(c,2)) +
                                            a6 / 720 * (61 - 58 * lat_tan2 + lat_tan4 + 600 * c - 330 * E_P2)));

        if (latitude < 0)
            northing += 10000000;

        return new float[] { easting, northing, zone_number, (int)zone_letter };
    }

    public static ZONE_LETTERS latitude_to_zone_letter(float latitude)
    {
        var lat_mins = System.Enum.GetValues(typeof(ZONE_LETTERS));
        foreach(int lat_min in lat_mins)
        {
            if (latitude >= lat_min)
            {
                return (ZONE_LETTERS)lat_min;
            }
        }
        return ZONE_LETTERS.None;
    }

    public static int latlon_to_zone_number(float latitude, float longitude)
    {
        if (56 <= latitude && latitude <= 64 && 3 <= longitude && longitude <= 12 )
            return 32;

        if (72 <= latitude && latitude <= 84 && longitude >= 0)
        {
            if (longitude <= 9)
                return 31;
            else if (longitude <= 21)
                return 33;
            else if (longitude <= 33)
                return 35;
            else if (longitude <= 42)
                return 37;
        }

        return (int)((longitude + 180) / 6) + 1;
    }

    static float zone_number_to_central_longitude(int zone_number)
    {
        return (zone_number - 1) * 6 - 180 + 3;
    }

}