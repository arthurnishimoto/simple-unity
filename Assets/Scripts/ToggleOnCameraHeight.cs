﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleOnCameraHeight : MonoBehaviour {

    [SerializeField]
    GameObject[] objects;

    bool showingObjects;

	// Update is called once per frame
	void Update () {
        float cameraHeight = Camera.main.transform.position.y;

        if(cameraHeight > 100 && !showingObjects)
        {
            foreach(GameObject g in objects)
            {
                g.SetActive(true);
            }
            showingObjects = true;
        }
        else if (cameraHeight < 0 && showingObjects)
        {
            foreach (GameObject g in objects)
            {
                g.SetActive(false);
            }
            showingObjects = false;
        }
    }
}
