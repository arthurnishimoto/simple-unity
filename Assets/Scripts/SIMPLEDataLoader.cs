﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SIMPLEDataLoader : MonoBehaviour {
    // File
    ArrayList validFileList;
    ArrayList fwdSonarFileList = new ArrayList();
    ArrayList upSonarFileList = new ArrayList();
    ArrayList stateFileList = new ArrayList();
    ArrayList ctdFileList = new ArrayList();

    public string dataPath;

    string[] dataFilePaths;
    public bool forceReload = false;
    public bool loadFwdFacingSonar = true;
    public bool loadUpperFacingSonar = false;
    public bool loadState = false;
    public bool startWithoutStartupUI = false;
    bool loadingFromCache = false;

    int validFilesFound;
    int dataFilesLoaded;

    public float stateDataResolution = 1.0f;

    // GUI
    private float progress = 0;
    public bool loaded = false;

    public ProgressBarCanvasGUI overallProgressBar;
    public ProgressBarCanvasGUI currentProgressBar1;
    public ProgressBarCanvasGUI currentProgressBar2;
    public ProgressBarCanvasGUI currentProgressBar3;

    Vector3 worldPosition = Vector3.zero;
    Vector3 worldOrientation = new Vector3(90, 90, 0);
    Vector3 worldScale = new Vector3(1, -1, 1);
    GameObject dataManagerObject;
    bool usingCachedData = false;

    public UnityEngine.UI.InputField dataPathInputUI;
    public UnityEngine.UI.InputField stateResolutionInputUI;
    public Transform configurationUI;

    string displayNodeImagePath = "C:/ProgramData/Mechdyne/Cluster Tools Service/plugins_data";
    string masterNodeImagePath = "C:/ProgramData/Mechdyne/Cluster Tools Service/plugins_data_client";
    //string masterNodeImagePath = "C:/data";

    // Use this for initialization
    void Start() {
        if (CAVE2.OnCAVE2Display())
        {
            dataPath = displayNodeImagePath + dataPath;
        }
        else
        {
            dataPath = masterNodeImagePath + dataPath;
        }

        if (PlayerPrefs.GetString("dataPath").Length > 0)
        {
            //dataPath = PlayerPrefs.GetString("dataPath");
        }

        if (PlayerPrefs.GetFloat("stateDataResolution") != 0)
        {
            stateDataResolution = PlayerPrefs.GetFloat("stateDataResolution");
        }

        if (dataPathInputUI)
            dataPathInputUI.text = dataPath;
        if (stateResolutionInputUI)
            stateResolutionInputUI.text = stateDataResolution.ToString();

        // Create Resources folder
        createFolders();

        if (startWithoutStartupUI)
        {
            StartLoad();
        }
    }

    public void SetDataPath(string value)
    {
        dataPath = value;
        PlayerPrefs.SetString("dataPath", dataPath);
    }

    public void StartLoad()
    {
       
        TimestampManager timeManager = GetComponent<TimestampManager>();
        SIMPLEPointCloudManager pointManager = GetComponent<SIMPLEPointCloudManager>();

        // Load pointclouds from Resources
        GameObject[] pointPrefabs = Resources.LoadAll<GameObject>("PointCloudMeshes");
        foreach (GameObject prefab in pointPrefabs)
        {
            GameObject pointGroup = Instantiate(prefab) as GameObject;

            // Removes (Clone) which was appended to the name
            pointGroup.name = pointGroup.name.Remove(pointGroup.name.Length - 7);

            pointGroup.transform.parent = GameObject.Find("World").transform;
            pointGroup.transform.localPosition = Vector3.zero;
            pointGroup.transform.localRotation = Quaternion.identity;
            pointGroup.transform.localScale = Vector3.one;
            MeshRenderer[] renderers = pointGroup.GetComponentsInChildren<MeshRenderer>();

            foreach (MeshRenderer mr in renderers)
            {
                mr.material = pointManager.matVertex;
                if (prefab.name.Contains("sonar"))
                {
                    pointManager.AddSonarRenderer(mr);
                }
                else if (prefab.name.Contains("ctd"))
                {
                    pointManager.AddCTDRenderer(mr);
                }
            }
        }
        
        /*
        TextAsset[] dataFiles = Resources.LoadAll<TextAsset>("Data");
        foreach (TextAsset dataFile in dataFiles)
        {
            Debug.Log(dataFile.name);
        }
        */
        try
        {
            dataFilePaths = Directory.GetFiles(dataPath);
        }
        catch (DirectoryNotFoundException)
        {
            dataFilePaths = Directory.GetFiles(Application.dataPath + "/Resources/Data/");
        }

        if (configurationUI)
            configurationUI.gameObject.SetActive(false);

        // Get Filenames
        validFileList = new ArrayList();
        foreach (string filePath in dataFilePaths)
        {
            // Testing subset
            //if (!filePath.Contains("2015-11-10"))
            //    continue;

            if (filePath.EndsWith(".meta"))
            {
                continue;
            }
            if (filePath.EndsWith("Fwd.xyzb"))
            {
                if (!loadFwdFacingSonar)
                    continue;
                fwdSonarFileList.Add(filePath);
                validFileList.Add(filePath);
            }
            if (filePath.EndsWith("Up.xyzb"))
            {
                if (!loadUpperFacingSonar)
                    continue;
                upSonarFileList.Add(filePath);
                validFileList.Add(filePath);
            }
            if (filePath.EndsWith("state.xyzb") || filePath.EndsWith("state.stateData"))
            {
                if (!loadState)
                    continue;
                stateFileList.Add(filePath);
                validFileList.Add(filePath);
            }
            if (filePath.EndsWith(".ctdOxygenData"))
            {
                if (!loadState)
                    continue;
                ctdFileList.Add(filePath);
                validFileList.Add(filePath);
            }
            // Testing case with subset of data
            //if (validFileList.Count > 3 * 5)
            //   break;

            //validFileList.Add(filePath);
        }

        validFilesFound = validFileList.Count;
        Debug.Log("validFilesFound: " + validFilesFound);

        if (!usingCachedData)
        {
            //StartCoroutine("loadData", fwdSonarFileList);
            //StartCoroutine("loadData", upSonarFileList);
            //StartCoroutine("loadData", stateFileList);
            StartCoroutine("loadData", validFileList);
        }
        
    }

    bool checkIfAssetExists(string filePath)
    {

        // Check if the PointCloud was loaded previously
        if (!Directory.Exists(Application.dataPath + "/Resources/PointCloudMeshes/" + Path.GetFileName(filePath)))
        {
#if UNITY_EDITOR
            UnityEditor.AssetDatabase.CreateFolder("Assets/Resources/PointCloudMeshes", Path.GetFileName(filePath));
#endif
            return false;
        }
        else if (forceReload)
        {
#if UNITY_EDITOR
            UnityEditor.FileUtil.DeleteFileOrDirectory(Application.dataPath + "/Resources/PointCloudMeshes/" + Path.GetFileName(filePath));
            UnityEditor.AssetDatabase.Refresh();
            UnityEditor.AssetDatabase.CreateFolder("Assets/Resources/PointCloudMeshes", Path.GetFileName(filePath));
#endif
            return false;
        }
        else
        {
            //Debug.Log(this.name + ": Found existing point cloud 'Assets/Resources/PointCloudMeshes/" + Path.GetFileName(filePath) + "'.");
            // Load stored PointCloud
            //loadStoredMeshes(Path.GetFileName(filePath));
            return true;
        }
    }

    void loadStoredMeshes(string filePath)
    {
        if (currentProgressBar1 && currentProgressBar2)
        {
            currentProgressBar1.label = "Loaded pre-generated forward sonar points";
            currentProgressBar1.progress = 1;
            currentProgressBar2.label = "Loaded pre-generated upper sonar points";
            currentProgressBar2.progress = 1;
        }

        Object res = Resources.Load("PointCloudMeshes/" + filePath);
        if (res != null && !GameObject.Find(filePath) )
        {
            GameObject pointGroup = Instantiate(res) as GameObject;

            // Removes (Clone) which was appended to the name
            pointGroup.name = pointGroup.name.Remove(pointGroup.name.Length - 7);

            pointGroup.transform.parent = GameObject.Find("World").transform;
            pointGroup.transform.localPosition = Vector3.zero;
            pointGroup.transform.localRotation = Quaternion.identity;
            pointGroup.transform.localScale = Vector3.one;
            MeshRenderer[] renderers = pointGroup.GetComponentsInChildren<MeshRenderer>();

            TimestampManager timeManager = GetComponent<TimestampManager>();
            SIMPLEPointCloudManager pointManager = GetComponent<SIMPLEPointCloudManager>();
            foreach (MeshRenderer mr in renderers)
            {
                mr.material = pointManager.matVertex;
                if (filePath.Contains("sonar"))
                {
                    pointManager.AddSonarRenderer(mr);
                }
                else if (filePath.Contains("ctd"))
                {
                    pointManager.AddCTDRenderer(mr);
                }
            }
        }
    }

    void ResetProgressBars()
    {
        if (!currentProgressBar1)
            return;

        currentProgressBar1.label = "";
        currentProgressBar2.label = "";
        currentProgressBar3.label = "";

        currentProgressBar1.progress = 0;
        currentProgressBar2.progress = 0;
        currentProgressBar3.progress = 0;
    }

    int loadersRunning;
    IEnumerator loadData(ArrayList validFileList)
    {
        foreach (string filePath in validFileList)
        {
            Debug.Log(this.GetType().Name + ": " + Path.GetFileName(filePath) + " found.");
            string missionName = Path.GetFileName(filePath).Substring(0, 22);

            if (!SIMPLEDataManager.ContainsMission(missionName))
            {
                //Debug.Log(this.GetType().Name + ": Generating data structure for " + missionName);
                SIMPLEDataManager.SIMPLEData simpleDataFile = new SIMPLEDataManager.SIMPLEData(missionName, filePath);
                SIMPLEDataManager.AddMission(missionName, simpleDataFile);
                ResetProgressBars();
                yield return new WaitUntil(() => loadersRunning == 0);
            }

            if (filePath.Contains(".stateData"))
            {
                //Debug.Log(this.GetType().Name + ": Adding stateData to " + missionName);
                //loadStateData(filePath);
                StartCoroutine("loadStateData", filePath);
                loadersRunning++;
            }

            if (filePath.EndsWith(".xyzb"))
            {
                //Debug.Log(this.GetType().Name + ": Adding sonarData to " + missionName);
                //loadStateData(loadSonarXYZB, filePath);
                if (checkIfAssetExists(filePath))
                {
                    loadStoredMeshes(filePath);
                }
                else
                {
                    StartCoroutine("loadSonarXYZB", filePath);
                    loadersRunning++;
                }
            }

            if (filePath.EndsWith(".ctdOxygenData"))
            {
                StartCoroutine("loadCTDData", filePath);
                loadersRunning++;
            }

            dataFilesLoaded++;

            if (overallProgressBar)
            {
                float progress = dataFilesLoaded * 1.0f / (validFilesFound) * 1.0f;
                overallProgressBar.progress = progress;
                overallProgressBar.label = "Loading " + missionName;
            }
            //guiText = dataFilesLoaded.ToString() + " out of " + validFilesFound.ToString() + " loaded";

            yield return null;

            if (dataFilesLoaded == validFilesFound)
            {
                loaded = true;

                if (overallProgressBar)
                {
                    overallProgressBar.gameObject.SetActive(false);
                    currentProgressBar1.gameObject.SetActive(false);
                    currentProgressBar2.gameObject.SetActive(false);
                    currentProgressBar3.gameObject.SetActive(false);
                }
                /*
                if (!Directory.Exists(Application.dataPath + "/Resources/DataManager.prefab"))
                {
                    Debug.Log("Saved data cache as " + Application.dataPath + "/Resources/DataManager.prefab");
                    UnityEditor.PrefabUtility.CreatePrefab("Assets/Resources/DataManager.prefab", dataManagerObject);
                }
                */


            }
        }

        foreach (string filePath in validFileList)
        {
            string missionName = Path.GetFileName(filePath).Substring(0, 22);
            GenerateCTDPointCloud(missionName);
        }
    }

    public bool HasMissionData(string missionName)
    {
        return SIMPLEDataManager.ContainsMission(missionName);
    }

    public SIMPLEDataManager.SIMPLEData GetMissionByName(string missionName)
    {
        return SIMPLEDataManager.GetMissionByName(missionName);
    }

    void createFolders()
    {
#if UNITY_EDITOR
        if (!Directory.Exists(Application.dataPath + "/Resources/"))
            UnityEditor.AssetDatabase.CreateFolder("Assets", "Resources");

        if (!Directory.Exists(Application.dataPath + "/Resources/PointCloudMeshes/"))
            UnityEditor.AssetDatabase.CreateFolder("Assets/Resources", "PointCloudMeshes");
#endif
    }

    // Start Coroutine of reading the data from the .stateData file
    // .stateData are binary files containing the vehicle state information
    // Data is stored as float values in the following format:
    // [position x, y, z][timestamp][orientation x, y, z, w][velocity x, y, z][angular velocity x, y, z][acceleration x, y, z][angular acceleration x, y, z]
    IEnumerator loadStateData(string dPath)
    {
        int dataFields = 20;

        FileInfo fileInfo = new FileInfo(dPath);
        int calcuatedPointCount = (int)((fileInfo.Length) / (sizeof(double) * dataFields));

        FileStream fs = null;

        // If file is opened by another application instance
        // wait until unlocked
        bool fileBusy = true;
        while (fileBusy)
        {
            try
            {
                fs = File.Open(dPath, FileMode.Open);
                fileBusy = false;
            }
            catch (IOException)
            {
                fileBusy = true;
                Debug.Log(dPath + " is in use");
            }

            if (fileBusy)
                yield return new WaitForSeconds(0.5f);
        }
        BinaryReader reader = new BinaryReader(fs);
       
        int numPoints = 0;
        SortedDictionary<long, SIMPLEDataManager.SIMPLEState> missionDataStates = new SortedDictionary<long, SIMPLEDataManager.SIMPLEState>();

        float progress;
        int incrementer = (int)(1 / stateDataResolution);

        for (int i = 0; i < calcuatedPointCount; i++)
        {
            try
            {
                if (i % incrementer != 0)
                {
                    reader.ReadBytes(sizeof(double) * dataFields);
                    continue;
                }

                double x = reader.ReadDouble();
                double y = reader.ReadDouble();
                double z = reader.ReadDouble();

                double timestamp = reader.ReadDouble();

                double rx = reader.ReadDouble();
                double ry = reader.ReadDouble();
                double rz = reader.ReadDouble();
                double rw = reader.ReadDouble();

                double vx = reader.ReadDouble();
                double vy = reader.ReadDouble();
                double vz = reader.ReadDouble();

                double avx = reader.ReadDouble();
                double avy = reader.ReadDouble();
                double avz = reader.ReadDouble();

                double ax = reader.ReadDouble();
                double ay = reader.ReadDouble();
                double az = reader.ReadDouble();

                double aax = reader.ReadDouble();
                double aay = reader.ReadDouble();
                double aaz = reader.ReadDouble();

                Vector3 position = new Vector3((float)x, (float)y, (float)z);
                Quaternion orientation = new Quaternion((float)rx, (float)ry, (float)rz, (float)rw);
                Vector3 velocity = new Vector3((float)vx, (float)vy, (float)vz);
                Vector3 angularVelocity = new Vector3((float)avx, (float)avy, (float)avz);
                Vector3 acceleration = new Vector3((float)ax, (float)ay, (float)az);
                Vector3 angularAcceleration = new Vector3((float)aax, (float)aay, (float)aaz);

                SIMPLEDataManager.SIMPLEState state = new SIMPLEDataManager.SIMPLEState();
                state.timestamp = (long)timestamp;
                state.position = position;
                state.orientation = orientation;
                state.velocity = velocity;
                state.angularVelocity = angularVelocity;
                state.angularAcceleration = angularAcceleration;

                missionDataStates.Add((long)timestamp, state);
                numPoints++;
            }
            catch
            {
                //Debug.Log("End of file");
                reader.Close();
                break;
            }

            if (numPoints % 2000 == 0 && dataFilesLoaded > 3)
            {
                progress = numPoints * 1.0f / (calcuatedPointCount * stateDataResolution) * 1.0f;
                //currentProgressBar3.progress = progress;
                //currentProgressBar3.label = "Loading state data " + numPoints.ToString() + "/" + (calcuatedPointCount * stateDataResolution).ToString() + "";
                //guiText = dataFilesLoaded.ToString() + " out of " + validFilesFound.ToString() + " loaded";
                yield return null;
            }
        }

        string missionName = Path.GetFileName(dPath).Substring(0, 22);
        Debug.Log(this.GetType().Name + ": Loaded " + numPoints + " state positions for mission '" + missionName + "'");
        SIMPLEDataManager.SIMPLEData data = GetMissionByName(missionName);
        data.SetStateData(missionDataStates);
        SIMPLEDataManager.UpdateMissionData(missionName, data);

        if (currentProgressBar3)
        {
            currentProgressBar3.progress = 1;
            currentProgressBar3.label = "Loaded state data " + numPoints.ToString() + "/" + calcuatedPointCount.ToString() + "";
        }
        loadersRunning--;

        GetComponent<SIMPLEVehicleManager>().GenerateStateLines(data);
    }

    // Start Coroutine of reading the points from the XYZB file and creating the meshes
    IEnumerator loadSonarXYZB(string dPath)
    {
        int dataFields = 7;

        FileInfo fileInfo = new FileInfo(dPath);
        int calcuatedPointCount = (int)(fileInfo.Length / (sizeof(double) * dataFields));
        string missionName = Path.GetFileName(dPath).Substring(0, 22);

        int type = -1;
        if (dPath.EndsWith("Up.xyzb"))
            type = 1;
        else if (dPath.EndsWith("Fwd.xyzb"))
            type = 0;

        BinaryReader reader = new BinaryReader(File.Open(dPath, FileMode.Open));
        int numPoints = 0;
        ArrayList pointsAL = new ArrayList();
        ArrayList colorsAL = new ArrayList();

        float progress;
        while (true)
        {
            try
            {
                double x = reader.ReadDouble();
                double y = reader.ReadDouble();
                double z = reader.ReadDouble();

                double r = reader.ReadDouble();
                double g = reader.ReadDouble();
                double b = reader.ReadDouble();
                double a = reader.ReadDouble();

                pointsAL.Add(new Vector3((float)x, (float)y, (float)z));
                colorsAL.Add(new Color((float)r, (float)g, (float)b, (float)a));

                numPoints++;
            }
            catch
            {
                //Debug.Log("End of file");
                reader.Close();
                break;
            }
            if (numPoints % 2000 == 0 && type == 0 && dataFilesLoaded > 3)
            {
                progress = numPoints * 1.0f / (calcuatedPointCount) * 1.0f;
                //currentProgressBar1.progress = progress;
                //currentProgressBar1.label = "Loading forward sonar points " + numPoints.ToString() + "/" + calcuatedPointCount.ToString() + "";
                //guiText = dataFilesLoaded.ToString() + " out of " + validFilesFound.ToString() + " loaded";
                yield return null;
            }
            else if (numPoints % 2000 == 0 && type == 1 && dataFilesLoaded > 3)
            {
                progress = numPoints * 1.0f / (calcuatedPointCount) * 1.0f;
                //currentProgressBar2.progress = progress;
                //currentProgressBar2.label = "Loading upper sonar points " + numPoints.ToString() + "/" + calcuatedPointCount.ToString() + "";
                //guiText = dataFilesLoaded.ToString() + " out of " + validFilesFound.ToString() + " loaded";
                yield return null;
            }
        }

        if (type == 0 && currentProgressBar1)
        {
            currentProgressBar1.progress = 1;
            currentProgressBar1.label = "Loaded forward sonar points " + numPoints.ToString() + "/" + calcuatedPointCount.ToString() + "";
        }
        else if (type == 1 && currentProgressBar2)
        {
            currentProgressBar2.progress = 1;
            currentProgressBar2.label = "Loaded upper sonar points " + numPoints.ToString() + "/" + calcuatedPointCount.ToString() + "";
        }

        //Debug.Log("Loaded " + numPoints + " points from " + Path.GetFileName(dPath));
        Vector3[] points = (Vector3[])pointsAL.ToArray(typeof(Vector3));
        Color[] colors = (Color[])colorsAL.ToArray(typeof(Color));


        SIMPLEDataManager.SIMPLEData data = GetMissionByName(missionName);
        SIMPLEDataManager.PointCloudData pointData = data.SetSonarData(dPath, points, colors, type);
        SIMPLEDataManager.UpdateMissionData(missionName, data);
        loadersRunning--;

        GetComponent<SIMPLEPointCloudManager>().StartCoroutine("GeneratePointCloud", pointData);

    }
    // -------------------------------------------------------------------------------------------------------------------
    /*
    void OnGUI()
    {
        if (!loaded)
        {
            GUI.BeginGroup(new Rect(Screen.width / 2 - 100, Screen.height / 2, 400.0f, 20));
            GUI.Box(new Rect(0, 0, 200.0f, 20.0f), guiText);
            GUI.Box(new Rect(0, 0, progress * 200.0f, 20), "");
            GUI.EndGroup();
        }
    }
    */

    public void SetStateDataResolution(string valueStr)
    {
        float value = stateDataResolution;
        if (float.TryParse(valueStr, out value))
        {
            if (value >= 0 && value <= 1.0f)
            {
                stateDataResolution = value;
                PlayerPrefs.SetFloat("stateDataResolution", stateDataResolution);
            }
            else
            {
                stateResolutionInputUI.text = stateDataResolution.ToString();
            }
        }
        else
        {
            stateResolutionInputUI.text = stateDataResolution.ToString();
        }
    }

    // Start Coroutine of reading the data from the .ctdData.ART.SB file
    // .ctdData are binary files containing the ctdData state information
    // Data is stored as float values in the following format:
    // [conductivity][salinity][temperature][timestamp][pressure][concentration][precentSaturation]
    IEnumerator loadCTDData(string dPath)
    {
        int dataFields = 7;

        FileInfo fileInfo = new FileInfo(dPath);
        int calcuatedPointCount = (int)((fileInfo.Length) / (sizeof(double) * dataFields));

        FileStream fs = null;

        // If file is opened by another application instance
        // wait until unlocked
        bool fileBusy = true;
        while (fileBusy)
        {
            try
            {
                fs = File.Open(dPath, FileMode.Open);
                fileBusy = false;
            }
            catch (IOException)
            {
                fileBusy = true;
                Debug.Log(dPath + " is in use");
            }

            if (fileBusy)
                yield return new WaitForSeconds(0.5f);
        }

        BinaryReader reader = new BinaryReader(fs);
        int numPoints = 0;
        SortedDictionary<long, SIMPLEDataManager.SIMPLECTD> missionDataStates = new SortedDictionary<long, SIMPLEDataManager.SIMPLECTD>();

        float progress;
        int incrementer = (int)(1 / stateDataResolution);
        for (int i = 0; i < calcuatedPointCount; i++)
        {
            //Debug.Log("Line " + i + " of " + calcuatedPointCount);

            try
            {
                if (i % incrementer != 0)
                {
                    reader.ReadBytes(sizeof(double) * dataFields);
                    continue;
                }

                double conductivity = reader.ReadDouble();
                double salinity = reader.ReadDouble();
                double temperature = reader.ReadDouble();
                double timestamp = reader.ReadDouble();
                double pressure = reader.ReadDouble();
                double concentration = reader.ReadDouble();
                double precentSaturation = reader.ReadDouble();

                SIMPLEDataManager.SIMPLECTD state = new SIMPLEDataManager.SIMPLECTD();
                state.timestamp = (long)timestamp;
                state.conductivity = (float)conductivity;
                state.salinity = (float)salinity;
                state.temperature = (float)temperature;
                state.pressure = (float)pressure;

                state.oxygenConcentration = (float)concentration;
                state.oxygenPercentSaturation = (float)precentSaturation;

                missionDataStates.Add((long)timestamp, state);
                numPoints++;
            }
            catch
            {
                //Debug.Log("End of file");
                reader.Close();
                break;
            }

            if (numPoints % 2000 == 0 && dataFilesLoaded > 3 && currentProgressBar3)
            {
                progress = numPoints * 1.0f / (calcuatedPointCount * stateDataResolution) * 1.0f;
                currentProgressBar3.progress = progress;
                currentProgressBar3.label = "Loading CTD data " + numPoints.ToString() + "/" + (calcuatedPointCount * stateDataResolution).ToString() + "";
                //guiText = dataFilesLoaded.ToString() + " out of " + validFilesFound.ToString() + " loaded";
                yield return null;
            }
        }

        string missionName = Path.GetFileName(dPath).Substring(0, 22);
        Debug.Log(this.GetType().Name + ": Loaded " + numPoints + " CTD positions for mission '" + missionName + "'");
        SIMPLEDataManager.SIMPLEData data = GetMissionByName(missionName);
        data.SetCTDData(missionDataStates);
        SIMPLEDataManager.UpdateMissionData(missionName, data);

        if (currentProgressBar3)
        {
            currentProgressBar3.progress = 1;
            currentProgressBar3.label = "Loaded CTD data " + numPoints.ToString() + "/" + calcuatedPointCount.ToString() + "";
        }
        loadersRunning--;

        //GetComponent<SIMPLEVehicleManager>().GenerateStateLines(data);
    }

    void GenerateCTDPointCloud(string missionName)
    {
        Debug.Log("GenerateCTDPointCloud: " + missionName);
        SIMPLEDataManager.SIMPLEData data = GetMissionByName(missionName);

        SortedDictionary<long, SIMPLEDataManager.SIMPLECTD> ctd = data.GetCTDData();

        // Get list of all ctd keys (timestamps)
        SortedDictionary<long, SIMPLEDataManager.SIMPLECTD>.KeyCollection keys = ctd.Keys;

        Vector3[] pos = new Vector3[keys.Count];
        Color[] col = new Color[keys.Count]; // conductivity, salinity, temperature
        Color[] col2 = new Color[keys.Count]; // pressure, oxygenConcentration, oxygenPercentSaturation

        int index = 0;
        foreach (long timestamp in keys)
        {
            // Get the vehicle position for each timestamp in the CTD data
            // Note this function must be called after state and CTD data is loaded
            pos[index] = data.GetPositionAtTimestamp(timestamp);
            col[index] = new Color(ctd[timestamp].conductivity, ctd[timestamp].salinity, ctd[timestamp].temperature);
            col2[index] = new Color(ctd[timestamp].pressure, ctd[timestamp].oxygenConcentration, ctd[timestamp].oxygenPercentSaturation);

            index++;
        }

        
        string filepath = missionName + "-artemis-ctd.xyzb";
        string filepath2 = missionName + "-artemis-ctdOxygen.xyzb";
        SIMPLEDataManager.PointCloudData ctdData1 = new SIMPLEDataManager.PointCloudData(filepath, pos, col, SIMPLEPointCloudManager.PointCloudType.CTD);
        SIMPLEDataManager.PointCloudData ctdData2 = new SIMPLEDataManager.PointCloudData(filepath2, pos, col2, SIMPLEPointCloudManager.PointCloudType.CTD);

        if( !checkIfAssetExists(filepath) )
        {
            GetComponent<SIMPLEPointCloudManager>().StartCoroutine("GeneratePointCloud", ctdData1);
        }
        if (!checkIfAssetExists(filepath2))
        {
            GetComponent<SIMPLEPointCloudManager>().StartCoroutine("GeneratePointCloud", ctdData2);
        }
    }
}
