﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleMeshRenderer : MonoBehaviour {

	public void Toggle()
    {
        if(GetComponent<MeshRenderer>())
        {
            GetComponent<MeshRenderer>().enabled = !GetComponent<MeshRenderer>().enabled;
        }
    }
}
