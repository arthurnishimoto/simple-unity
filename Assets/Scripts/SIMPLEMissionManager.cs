﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SIMPLEMissionManager : MonoBehaviour {

    public string missionDate = "2015-11-10";
    public GameObject sonarFwd;
    public GameObject sonarUp;
    public GameObject statePosition;
    public GameObject ctd1;
    public GameObject ctd2;

    public bool showFwdSonar;
    public bool showUpSonar;
    public bool showStatePosition;
    public bool showCTD;
    bool showCTD1;
    bool showCTD2;

    bool initFwdSonar = false;
    bool initUpSonar = false;
    bool initCTD1 = false;
    bool initCTD2 = false;

    public MissionSelectUI uiController;
    PointerEventData pointer = new PointerEventData(EventSystem.current);

    Hashtable missionTimestamps = new Hashtable();
    public TimestampManager timeManager;
    SIMPLEPointCloudManager pointManager;

    SIMPLEMissionUIManager missionUIManager;

    Vector3[] originalPositionLine;

    // Use this for initialization
    void Start () {
        if (uiController)
        {
            uiController.SetMissionManager(this);
        }

        // start end of state timestamp
        missionTimestamps["2015-11-10"] = new Vector2( 1447136652300000, 1447148266580000);
        missionTimestamps["2015-11-11"] = new Vector2( 1447222559940000, 1447241205200000);
	    missionTimestamps["2015-11-13"] = new Vector2( 1447405268550000, 1447419693890000);
	    missionTimestamps["2015-11-15"] = new Vector2( 1447559288410000, 1447590528950000);
	    missionTimestamps["2015-11-17"] = new Vector2( 1447742444630000, 1447770554340000);
	    missionTimestamps["2015-11-20"] = new Vector2( 1448000943020000, 1448028135540000);
	    missionTimestamps["2015-11-23"] = new Vector2( 1448254583420000, 1448261235750000);
	    missionTimestamps["2015-11-25"] = new Vector2( 1448422320550000, 1448438942410000);
	    missionTimestamps["2015-11-27"] = new Vector2( 1448581368810000, 1448614398800000);
	    missionTimestamps["2015-11-30"] = new Vector2( 1448845094170000, 1448855032910000);
	    missionTimestamps["2015-12-03"] = new Vector2( 1449103681110000, 1449140823150000);
	    missionTimestamps["2015-12-05"] = new Vector2( 1449270766930000, 1449270766930000);
        missionTimestamps["2015-12-08"] = new Vector2( 1449531292760000, 1449561044210000);

        timeManager = GameObject.Find("Data Manager").GetComponent<TimestampManager>();
        pointManager = GameObject.Find("Data Manager").GetComponent<SIMPLEPointCloudManager>();
        missionUIManager = GameObject.Find("UI Manager").GetComponent<SIMPLEMissionUIManager>();
    }
	
	// Update is called once per frame
	void Update () {

        missionUIManager.SetFwdSonarState(missionDate, sonarFwd != null);
        missionUIManager.SetUpSonarState(missionDate, sonarUp != null);
        missionUIManager.SetStateState(missionDate, statePosition != null);
        missionUIManager.SetCTDState(missionDate, ctd1 != null && ctd2 != null);
        missionUIManager.SetAllMissionState(missionDate,
            sonarFwd != null &&
            sonarUp != null &&
            statePosition != null &&
            ctd1 != null && ctd2 != null
        );

        if (uiController)
        {
            uiController.mission = missionDate;
            uiController.missionButtonLabel.text = missionDate;
            uiController.sonarFwdButton.interactable = sonarFwd != null;
            uiController.sonarUpButton.interactable = sonarUp != null;
            uiController.stateButton.interactable = statePosition != null;

            uiController.missionButton.interactable = (statePosition) ? true : false;

            var colors = uiController.missionButton.colors;

            if ( timeManager.sonarColorMode == TimestampManager.SonarColorMode.Mission )
            {
                colors.normalColor = SIMPLEPointCloudManager.GetMissionColor(missionDate);   
            }
            else
            {
                colors.normalColor = Color.white;
            }
            uiController.missionButton.colors = colors;
        }

        if (sonarFwd == null)
        {
            sonarFwd = GameObject.Find("missionFile-"+ missionDate + "-artemis-sonarFwd.xyzb");
        }
        else
        {
            sonarFwd.SetActive(showFwdSonar);
            if (!initFwdSonar)
            {
                EnableRenderers(sonarFwd);
                initFwdSonar = true;
            }

            if (uiController && showFwdSonar)
            {
                uiController.sonarFwdButton.OnSelect(pointer);
            }
            else if (uiController)
            {
                uiController.sonarFwdButton.OnDeselect(pointer);
            }
        }
        if (sonarUp == null)
        {
            sonarUp = GameObject.Find("missionFile-" + missionDate + "-artemis-sonarUp.xyzb");
        }
        else
        {
            sonarUp.SetActive(showUpSonar);
            if (!initUpSonar)
            {
                EnableRenderers(sonarUp);
                initUpSonar = true;
            }

            if (showUpSonar && uiController)
            {
                uiController.sonarUpButton.OnSelect(pointer);
            }
            else if (uiController)
            {
                uiController.sonarUpButton.OnDeselect(pointer);
            }
        }

        if (ctd1 == null)
        {
            ctd1 = GameObject.Find("missionFile-" + missionDate + "-artemis-ctd.xyzb");
        }
        else
        {
            ctd1.SetActive(showCTD1 && showCTD);
            if (!initCTD1)
            {
                EnableRenderers(ctd1);
                initCTD1 = true;
            }

            /*
            if (showUpSonar && uiController)
            {
                uiController.sonarUpButton.OnSelect(pointer);
            }
            else if (uiController)
            {
                uiController.sonarUpButton.OnDeselect(pointer);
            }
            */
        }

        if (ctd2 == null)
        {
            ctd2 = GameObject.Find("missionFile-" + missionDate + "-artemis-ctdOxygen.xyzb");
        }
        else
        {
            ctd2.SetActive(showCTD2 && showCTD);
            if (!initCTD2)
            {
                EnableRenderers(ctd2);
                initCTD2 = true;
            }

            /*
            if (showUpSonar && uiController)
            {
                uiController.sonarUpButton.OnSelect(pointer);
            }
            else if (uiController)
            {
                uiController.sonarUpButton.OnDeselect(pointer);
            }
            */
        }

        if (statePosition == null)
        {
            statePosition = GameObject.Find("missionFile-" + missionDate + "-artemis-state.stateData");
        }
        else
        {
            statePosition.SetActive(showStatePosition);
            LineRenderer stateLine = statePosition.GetComponent<LineRenderer>();
            stateLine.enabled = true;

            // Update the UI state
            if (uiController && showStatePosition)
            {
                uiController.stateButton.OnSelect(pointer);
            }
            else if (uiController)
            {
                uiController.stateButton.OnDeselect(pointer);
            }
        }

        if(statePosition)
            ColorStatePositionByMission(timeManager.sonarColorMode == TimestampManager.SonarColorMode.Mission);
    }

    public void SetStateLineByMaxTimestamp(long timestamp)
    {
        Vector3[] positions = SIMPLEDataManager.GetMissionByDate(missionDate).GetStatePositions(SIMPLEVehicleManager.statePositionIncrements, timestamp);
        statePosition.GetComponent<LineRenderer>().SetVertexCount(positions.Length);
        statePosition.GetComponent<LineRenderer>().SetPositions(positions); 
    }

    public void SetStateLineByEntireDuration()
    {
        Vector3[] positions = SIMPLEDataManager.GetMissionByDate(missionDate).GetStatePositions(SIMPLEVehicleManager.statePositionIncrements);
        if (statePosition)
        {
            statePosition.GetComponent<LineRenderer>().SetVertexCount(positions.Length);
            statePosition.GetComponent<LineRenderer>().SetPositions(positions);
        }
    }

    public void ColorStatePositionByMission(bool value)
    {
        Color defaultLineColor = Color.white;
        ColorUtility.TryParseHtmlString("#E6C614", out defaultLineColor);

        if (value)
        {
            Color missionColor = SIMPLEPointCloudManager.GetMissionColor(missionDate);
            statePosition.GetComponent<LineRenderer>().material.color = missionColor;
            statePosition.GetComponent<LineRenderer>().material.SetColor("_EmissionColor", missionColor);
        }
        else
        {
            statePosition.GetComponent<LineRenderer>().material.color = defaultLineColor;
            statePosition.GetComponent<LineRenderer>().material.SetColor("_EmissionColor", defaultLineColor);
        }
    }

    void EnableRenderers(GameObject g)
    {
        MeshRenderer[] renderers = g.GetComponentsInChildren<MeshRenderer>();
        foreach(MeshRenderer r in renderers)
        {
            r.enabled = true;
        }
    }

    public void ToggleFwdSonar(bool value)
    {
        if (sonarFwd)
        {
            showFwdSonar = value;
        }
    }

    public void ToggleUpSonar(bool value)
    {
        if (sonarUp)
        {
            showUpSonar = value;
        }
    }

    public void ToggleState(bool value)
    {
        if (statePosition)
        {
            showStatePosition = value;
        }
    }

    public void ToggleCTD(bool value)
    {
        showCTD = value;
    }

    public void ToggleFwdSonarDisplay()
    {
        if (sonarFwd)
        {
            showFwdSonar = !showFwdSonar;
            sonarFwd.SetActive(showFwdSonar);
        }
    }

    public void ToggleUpSonarDisplay()
    {
        if (sonarUp)
        {
            showUpSonar = !showUpSonar;
            sonarUp.SetActive(showUpSonar);
        }
    }

    public void ToggleDisplayState()
    {
        if (statePosition)
        {
            showStatePosition = !showStatePosition;
            statePosition.SetActive(showStatePosition);
        }
    }

    public void SetCurrentMission(string missionDate)
    {
        Vector2 timeRanges = Vector2.zero;
        if (missionTimestamps[missionDate] != null)
            timeRanges = (Vector2)missionTimestamps[missionDate];
        timeManager.SetCurrentMission(missionDate, (long)timeRanges.x, (long)timeRanges.y);
    }

    public void ShowAll()
    {
        showFwdSonar = true;
        showUpSonar = true;
        showStatePosition = true;
        showCTD = true;
        showCTD1 = true;
        showCTD2 = true;
        SetCurrentMission(missionDate);
    }

    public void HideAll()
    {
        showFwdSonar = false;
        showUpSonar = false;
        showStatePosition = false;
        showCTD = false;
        showCTD1 = false;
        showCTD2 = false;
    }

    public void ShowCTD1()
    {
        showCTD1 = true;
        showCTD2 = false;
    }

    public void ShowCTD2()
    {
        showCTD1 = false;
        showCTD2 = true;
    }

    public void HideCTD()
    {
        showCTD = false;
        showCTD1 = false;
        showCTD2 = false;
    }
}
