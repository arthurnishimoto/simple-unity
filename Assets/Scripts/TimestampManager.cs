﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.EventSystems;

public class TimestampManager : MonoBehaviour {

    public string currentMission = "2015-11-10";

    // Microseconds
    public long missionStart = 1447136662903310;
    public long missionEnd = 1447148266124440;

    public long currentTimestamp;

    public float currentTimeRatio;

    public string startDate;
    public string startTime;

    public string currentDate;
    public string currentTime;

    public string endDate;
    public string endTime;

    public enum SonarColorMode { Material, SensorType, Mission };
    public SonarColorMode sonarColorMode = SonarColorMode.SensorType;

    public enum CTDColorMode { Conductivity, Salinity, Temperature, Pressure, O2Concentration, O2PercentSaturation };
    public CTDColorMode ctdColorMode = CTDColorMode.Pressure;

    public bool colorByTimestamp = false;

    public enum SonarShape { Point, Cube };
    public SonarShape sonarShape = SonarShape.Cube;

    public bool setPositionBasedOnTime = false;
    public bool progressTime = false;
    float timeScale = 1.0f;
    public int speedFactor = 3;

    public UnityEngine.UI.Text timeframeTextUI;
    public UnityEngine.UI.Text timeframeStartTextUI;
    public UnityEngine.UI.Text timeframeEndTextUI;
    public UnityEngine.UI.Text vehicleOrientationTextUI;

    public UnityEngine.UI.Text speedTextUI;
    public UnityEngine.UI.Button followButton;

    public Vector3 pitchYawRollRaw;
    public Vector3 pitchYawRoll;

    public DateTime currentDateTime;

    public Navigator navigator;

    public UnityEngine.UI.Slider timeSliderUI;

    public UnityEngine.UI.Image playImageUI;
    public Sprite playSprite;
    public Sprite pauseSprite;

    public UnityEngine.UI.Image playIcon;
    public UnityEngine.UI.Image colorByTimeIcon;

    string vehicleCTDTextUI;

    public SortedDictionary<string, SIMPLEMissionManager> missionManagers = new SortedDictionary<string, SIMPLEMissionManager>();

    // Use this for initialization
    void Start () {
        startDate = timestampToDateTime(missionStart).ToShortDateString();
        startTime = timestampToDateTime(missionStart).ToShortTimeString();

        endDate = timestampToDateTime(missionEnd).ToShortDateString();
        endTime = timestampToDateTime(missionEnd).ToShortTimeString();

        UpdateSpeed();

        // Get all mission managers, store by mission date
        SIMPLEMissionManager[] missionManagerList = GameObject.Find("MissionManager").GetComponentsInChildren<SIMPLEMissionManager>();
        foreach (SIMPLEMissionManager mm in missionManagerList)
        {
            missionManagers[mm.missionDate] = mm;
        }
    }

    // Update is called once per frame
    void Update() {

        if (progressTime)
        {
            currentTimestamp += (long)(Time.deltaTime * 1000000 * timeScale);
            currentTimeRatio = (currentTimestamp - missionStart) / (float)(missionEnd - missionStart);

            if (playImageUI && pauseSprite)
                playImageUI.sprite = pauseSprite;
            if (playIcon)
                playIcon.enabled = true;
        }
        else
        {
            currentTimestamp = (long)Mathf.Lerp((float)missionStart, (float)missionEnd, currentTimeRatio);
            if (playImageUI && playSprite)
                playImageUI.sprite = playSprite;
            if (playIcon)
                playIcon.enabled = false;
        }

        if (!GetComponent<SIMPLEDataLoader>().loaded)
            return;

        if (setPositionBasedOnTime)
        {
            // Set vehicle position
            SIMPLEDataManager.SIMPLEData missionData = GetComponent<SIMPLEDataLoader>().GetMissionByName("missionFile-" + currentMission);
            navigator.transform.localPosition = missionData.GetPositionAtTimestamp(currentTimestamp);
            Quaternion orientation = missionData.GetOrientationAtTimestamp(currentTimestamp);
            navigator.transform.localRotation = orientation;
            pitchYawRollRaw = orientation.eulerAngles;
            pitchYawRoll = navigator.transform.eulerAngles;

            if (vehicleOrientationTextUI)
            {
                vehicleOrientationTextUI.text = "Pitch/Yaw/Roll(?): " + pitchYawRollRaw.x.ToString("F2") + " " + pitchYawRollRaw.z.ToString("F2") + " " + pitchYawRollRaw.x.ToString("F2") + " degrees";
            }

            //ctd.conductivity, ctd.salinity, ctd.temperature, ctd.pressure, ctd.oxygenConcentration, ctd.oxygenPercentSaturation
            float[] curCTDData = missionData.GetCTDAtTimestamp(currentTimestamp);
            vehicleCTDTextUI =
                "" + curCTDData[0].ToString("F3") +
                "\n" + curCTDData[1].ToString("F3") +
                "\n" + curCTDData[2].ToString("F3") +
                "\n" + curCTDData[3].ToString("F3") +
                "\n" + curCTDData[4].ToString("F3") +
                "\n" + curCTDData[5].ToString("F3") +
                "";

            // Set line length
            if(colorByTimestamp)
                missionManagers[currentMission].SetStateLineByMaxTimestamp(currentTimestamp);
            else
                missionManagers[currentMission].SetStateLineByEntireDuration();
        }
        else
        {
            vehicleCTDTextUI = "N/A";

            Quaternion orientation = Quaternion.Euler(new Vector3(-180, 90, 90));
            if (navigator)
            {
                navigator.transform.localRotation = orientation;
                pitchYawRollRaw = orientation.eulerAngles;
                pitchYawRoll = navigator.transform.eulerAngles;
            }

            if (vehicleOrientationTextUI)
            {
                vehicleOrientationTextUI.text = "Pitch/Yaw/Roll(?): " + pitchYawRollRaw.x.ToString("F2") + " " + pitchYawRollRaw.z.ToString("F2") + " " + pitchYawRollRaw.x.ToString("F2") + " degrees";
            }
        }

        if (followButton && setPositionBasedOnTime)
        {
            followButton.OnSelect(new BaseEventData(EventSystem.current));
            followButton.GetComponentInChildren<UnityEngine.UI.Text>().text = "Unfollow Vehicle";
        }
        else if (followButton)
        {
            followButton.OnDeselect(new BaseEventData(EventSystem.current));
            followButton.GetComponentInChildren<UnityEngine.UI.Text>().text = "Follow Vehicle";
        }

        currentDateTime = timestampToDateTime(currentTimestamp);
        currentDate = currentDateTime.Year + "-" + currentDateTime.Month + "-" + currentDateTime.Day;
        currentTime = currentDateTime.Hour + ":" + currentDateTime.Minute + ":" + currentDateTime.Second + "." + currentDateTime.Millisecond;

        if (timeframeTextUI)
        {
            timeframeTextUI.text = currentDate + " " + currentTime;
        }

        if (timeframeStartTextUI)
        {
            DateTime currentMissionTime = timestampToDateTime(currentTimestamp - missionStart);
            timeframeStartTextUI.text = currentMissionTime.Hour.ToString("D2") + ":" + currentMissionTime.Minute.ToString("D2") + ":" + currentMissionTime.Second.ToString("D2") + "." + currentMissionTime.Millisecond.ToString("D2");

            DateTime missionTimeRemain = timestampToDateTime(missionEnd - currentTimestamp);
            timeframeEndTextUI.text = missionTimeRemain.Hour.ToString("D2") + ":" + missionTimeRemain.Minute.ToString("D2") + ":" + missionTimeRemain.Second.ToString("D2") + "." + missionTimeRemain.Millisecond.ToString("D2");
        }

        GetComponent<SIMPLEPointCloudManager>().SetSIMPLEShaderVars(currentTimestamp, missionStart, (int)sonarColorMode, colorByTimestamp);

        if (colorByTimeIcon)
        {
            colorByTimeIcon.enabled = colorByTimestamp;
        }

        // CTD
        switch (sonarShape)
        {
            case (SonarShape.Point):
                GetComponent<SIMPLEPointCloudManager>().SetSonarRenderShape(1);
                break;
            case (SonarShape.Cube):
                GetComponent<SIMPLEPointCloudManager>().SetSonarRenderShape(0);
                break;
        }

        // CTD
        switch (ctdColorMode)
        {
            case (CTDColorMode.Pressure):
                GetComponent<SIMPLEPointCloudManager>().ColorCTDByPressure();
                break;
            case (CTDColorMode.Salinity):
                GetComponent<SIMPLEPointCloudManager>().ColorCTDBySalinity();
                break;
            case (CTDColorMode.Temperature):
                GetComponent<SIMPLEPointCloudManager>().ColorCTDByTemperature();
                break;
            case (CTDColorMode.Conductivity):
                GetComponent<SIMPLEPointCloudManager>().ColorCTDByConductivity();
                break;
            case (CTDColorMode.O2Concentration):
                GetComponent<SIMPLEPointCloudManager>().ColorCTDByO2Concentration();
                break;
            case (CTDColorMode.O2PercentSaturation):
                GetComponent<SIMPLEPointCloudManager>().ColorCTDByO2Saturation();
                break;
        }
    }

    public string GetCTDTextInfo()
    {
        return vehicleCTDTextUI;
    }

    public void SetCTDColorMode(int mode)
    {
        ctdColorMode = (CTDColorMode)mode;
    }

    DateTime timestampToDateTime(long epoch)
    {
        return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(epoch / 1000);
    }
   
    public void SetColorMode(int value)
    {
        switch(value)
        {
            case (0):
                sonarColorMode = SonarColorMode.Material;
                GetComponent<SIMPLEPointCloudManager>().ColorByMission(false);
                colorByTimestamp = false;
                break;
            case (1):
                sonarColorMode = SonarColorMode.SensorType;
                colorByTimestamp = false;
                break;
            case (2):
                sonarColorMode = SonarColorMode.Mission;
                colorByTimestamp = false;
                GetComponent<SIMPLEPointCloudManager>().ColorByMission(true);
                break;
        }
    }

    public void SetSonarRenderPoint(bool value)
    {
        if (value)
            sonarShape = SonarShape.Point;
    }

    public void SetSonarRenderCube(bool value)
    {
        if (value)
            sonarShape = SonarShape.Cube;
    }

    public void SetColorbyTimestamp(bool value)
    {
        colorByTimestamp = value;
    }

    public void SetCurrentMission(string missionDate, long startTime, long EndTime)
    {
        currentMission = missionDate;
        missionStart = startTime;
        missionEnd = EndTime;
        setPositionBasedOnTime = true;
        currentTimestamp = missionStart;
        /*
        foreach (SIMPLEMissionManager mm in missionManagers)
        {
            if( mm.missionDate == currentMission)
            {
                mm.ShowAll();
            }
            else
            {
                mm.HideAll();
            }
        }
        */
    }

    public void SetTimestampRatio(Single timeRatio)
    {
        currentTimeRatio = timeRatio;
        //timeSliderUI.value = currentTimeRatio;
        currentTimestamp = (long)Mathf.Lerp((float)missionStart, (float)missionEnd, currentTimeRatio);
    }

    public void TogglePlayback()
    {
        progressTime = !progressTime;
    }

    public void ToggleColorByTimestamp()
    {
        colorByTimestamp = !colorByTimestamp;
    }

    public void ToggleVehicleFollow()
    {
        setPositionBasedOnTime = !setPositionBasedOnTime;
    }

    public void IncreaseSpeed()
    {
        speedFactor++;
        UpdateSpeed();
    }

    public void DecreaseSpeed()
    {
        speedFactor--;
        UpdateSpeed();
    }

    public void SetSpeed(float value)
    {
        speedFactor = (int)value;
        UpdateSpeed();
    }

    void UpdateSpeed()
    {
        if (speedFactor > 6)
            speedFactor = 6;
        else if (speedFactor < -6)
            speedFactor = -6;

        switch (speedFactor)
        {
            case (0): timeScale = 1; break;
            case (1): timeScale = 5; break;
            case (2): timeScale = 10; break;
            case (3): timeScale = 25; break;
            case (4): timeScale = 50; break;
            case (5): timeScale = 100; break;
            case (6): timeScale = 250; break;
            case (-1): timeScale = -5; break;
            case (-2): timeScale = -10; break;
            case (-3): timeScale = -25; break;
            case (-4): timeScale = -50; break;
            case (-5): timeScale = -100; break;
            case (-6): timeScale = -250; break;
        }

        if (speedTextUI)
            speedTextUI.text = (int)timeScale + "x";
    }
}
