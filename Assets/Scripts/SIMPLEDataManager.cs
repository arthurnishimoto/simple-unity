﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SIMPLEDataManager : MonoBehaviour{

    static SortedDictionary<string, SIMPLEData> missionData = new SortedDictionary<string, SIMPLEDataManager.SIMPLEData>();
    public int missionEntries;

    // Mission name is in format: missionFile-[year]-[month]-[day]
    public static bool ContainsMission(string missionName)
    {
        return missionData.ContainsKey(missionName);
    }

    // Mission name is in format: missionFile-[year]-[month]-[day]
    public static SIMPLEData GetMissionByName(string missionName)
    {
        if (missionData.ContainsKey(missionName))
            return missionData[missionName];
        else
            return new SIMPLEData();
    }

    public static SIMPLEData GetMissionByDate(string missionDate)
    {
        if (missionData.ContainsKey("missionFile-" + missionDate))
            return missionData["missionFile-" + missionDate];
        else
            return new SIMPLEData();
    }

    public static void AddMission(string missionName, SIMPLEData data)
    {
        missionData.Add(missionName, data);

    }

    public static void UpdateMissionData(string missionName, SIMPLEData data)
    {
        missionData[missionName] = data;
    }

    public void Update()
    {
        missionEntries = missionData.Count;
    }

    public struct SIMPLERadar
    {
        public long timestamp;
        public Vector2 latLong;
        public float iceThickness;
        public float surfaceRange;
        public float bedElevation;
        public float surfaceElevation;
        //public float bedReflectionCE;
        //public float surfaceReflectionCE;
        //public float roll;
    }

    public struct SIMPLEState
    {
        public long timestamp;
        public Vector3 position;
        public Quaternion orientation;
        public Vector3 velocity;
        public Vector3 angularVelocity;
        public Vector3 accleration;
        public Vector3 angularAcceleration;
    }

    public struct SIMPLECTD
    {
        public long timestamp;
        public float conductivity;
        public float salinity;
        public float temperature;
        public float pressure;

        public float oxygenConcentration;
        public float oxygenPercentSaturation;
    }

    public struct PointCloudData
    {
        public SIMPLEPointCloudManager.PointCloudType type;
        public string filePath;
        public Vector3[] positions;
        public Color[] colors;
        public int nPoints;

        public PointCloudData(string s, Vector3[] p, Color[] c, SIMPLEPointCloudManager.PointCloudType t)
        {
            filePath = s;
            positions = p;
            colors = c;
            nPoints = p.Length;
            type = t;
        }
    }

    public struct SIMPLEData
    {
        string missionName;
        string filePath;

        SortedDictionary<long, SIMPLEState> stateData;
        List<long> stateDataKeys;
        List<SIMPLEState> stateValueList;

        SortedDictionary<long, SIMPLECTD> ctdData;
        List<long> ctdDataKeys;
        List<SIMPLECTD> ctdValueList;

        PointCloudData fwdSonarData;
        PointCloudData upSonarData;

        // Used to store positions for quick retrieval
        Vector3[] statePositions;

        int positionIncrements;
        Vector3[] statePositionsIncrements;

        public SIMPLEData(string name, string path)
        {
            missionName = name;
            filePath = path;

            // Sorted list of all state data (value) at a timestamp (key)
            // Good for quickly getting data from a particular timestamp
            stateData = new SortedDictionary<long, SIMPLEState>();
            stateDataKeys = new List<long>();
            stateValueList = new List<SIMPLEState>();

            ctdData = new SortedDictionary< long, SIMPLECTD> ();
            ctdDataKeys = new List<long>();
            ctdValueList = new List<SIMPLECTD>();

            fwdSonarData = new PointCloudData();
            upSonarData = new PointCloudData();

            statePositions = new Vector3[0];

            positionIncrements = 1;
            statePositionsIncrements = new Vector3[0];
        }

        public void SetPath(string path)
        {
            filePath = path;
        }

        // State data ----------------------------------------------------------------
        public void SetStateData(SortedDictionary<long, SIMPLEState> data)
        {
            stateData = data;
            stateDataKeys = new List<long>(stateData.Keys);
            stateValueList = new List<SIMPLEState>(stateData.Values);

            // Create a list of positions for faster parsing later
            statePositions = GetStatePositions();
        }

        public SortedDictionary<long, SIMPLEState> GetStateData()
        {
            return stateData;
        }

        public Vector3[] GetStatePositions()
        {
            Vector3[] statePositions = new Vector3[stateData.Count];
            int i = 0;
            foreach (KeyValuePair<long, SIMPLEState> state in stateData)
            {
                statePositions[i] = state.Value.position;
                i++;
            }
            return statePositions;
        }

        public Vector3[] GetStatePositions(int increments)
        {
            // Use previously generated list if same parameters
            if (increments == positionIncrements)
            {
                return statePositionsIncrements;
            }
            else
            {
                positionIncrements = increments;
                Vector3[] statePositions = new Vector3[stateData.Count / increments];
                int i = 0;
                int j = 0;
                foreach (KeyValuePair<long, SIMPLEState> state in stateData)
                {
                    if (j % increments == 0)
                    {
                        try
                        {
                            statePositions[i] = state.Value.position;
                            i++;
                        }
                        catch (System.IndexOutOfRangeException e)
                        { }
                    }
                    j++;
                }
                return statePositions;
            }
        }

        public Vector3[] GetStatePositions(int increments, long maxTimestamp)
        {
            ArrayList statePositions = new ArrayList();
            int i = 0;
            int j = 0;
            foreach (KeyValuePair<long, SIMPLEState> state in stateData)
            {
                if (j % increments == 0)
                {
                    try
                    {
                        if (state.Key <= maxTimestamp)
                        {
                            statePositions.Add(state.Value.position);
                            i++;
                        }
                        else
                        {
                            break;
                        }
                    }
                    catch (System.IndexOutOfRangeException e)
                    {
                    }
                }
                j++;
            }
            Vector3[] vecArray = new Vector3[i];
            statePositions.CopyTo(vecArray);
            return vecArray;
        }

        public Vector3 GetPositionAtTimestamp(long timestamp)
        {
            SIMPLEState state = new SIMPLEState();
            try
            {
                state = stateValueList[~stateDataKeys.BinarySearch(timestamp)];
            }
            catch (Exception)
            {
                //Debug.LogError(e.Message);
            }
            return state.position;
        }

        public Quaternion GetOrientationAtTimestamp(long timestamp)
        {
            SIMPLEState state = new SIMPLEState();
            try
            {
                state = stateValueList[~stateDataKeys.BinarySearch(timestamp)];
            }
            catch (Exception)
            {
                //Debug.LogError(e.Message);
            }
            return state.orientation;
        }

        // CTD Data ----------------------------------------------------------------
        public void SetCTDData(SortedDictionary<long, SIMPLECTD> data)
        {
            ctdData = data;
            ctdDataKeys = new List<long>(ctdData.Keys);
            ctdValueList = new List<SIMPLECTD>(ctdData.Values);
        }

        public SortedDictionary<long, SIMPLECTD> GetCTDData()
        {
            return ctdData;
        }

        public float[] GetCTDAtTimestamp(long timestamp)
        {
            SIMPLECTD ctd = new SIMPLECTD();
            try
            {
                ctd = ctdValueList[~ctdDataKeys.BinarySearch(timestamp)];
            }
            catch
            {
                //Debug.LogError(e.Message);
            }
            return new float[] { ctd.conductivity, ctd.salinity, ctd.temperature, ctd.pressure, ctd.oxygenConcentration, ctd.oxygenPercentSaturation };
        }

        // Sonar data ----------------------------------------------------------------
        // Type = 0: forward, 1: upper
        public PointCloudData SetSonarData(string filePath, Vector3[] points, Color[] colors, int type)
        {
            if (type == 0)
            {
                fwdSonarData = new PointCloudData(filePath, points, colors, SIMPLEPointCloudManager.PointCloudType.Sonar);
                return fwdSonarData;
            }
            else if (type == 1)
            {
                upSonarData = new PointCloudData(filePath, points, colors, SIMPLEPointCloudManager.PointCloudType.Sonar);
                return upSonarData;
            }
            return new PointCloudData();
        }

        public PointCloudData GetFwdSonarData()
        {
            return fwdSonarData;
        }

        public string GetFilePath()
        {
            return filePath;
        }
    }
}
