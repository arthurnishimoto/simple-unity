﻿using UnityEngine;
using System.Collections;

public class SIMPLEMissionUIManager : MonoBehaviour {

    [SerializeField]
    SIMPLEMissionController allMissions;

    [SerializeField]
    SIMPLEMissionController fwdSonarSelector;

    [SerializeField]
    SIMPLEMissionController upSonarSelector;

    [SerializeField]
    SIMPLEMissionController stateSelector;

    [SerializeField]
    SIMPLEMissionController ctdSelector;
	
	// Update is called once per frame
	void Update () {
	
	}

    int MissionDateToButtonID(string missionDate)
    {
        switch (missionDate)
        {
            case ("2015-11-10"): return 0;
            case ("2015-11-11"): return 1;
            case ("2015-11-13"): return 2;
            case ("2015-11-15"): return 3;
            case ("2015-11-17"): return 4;
            case ("2015-11-20"): return 5;
            case ("2015-11-23"): return 6;
            case ("2015-11-25"): return 7;
            case ("2015-11-27"): return 8;
            case ("2015-11-30"): return 9;
            case ("2015-12-03"): return 10;
            case ("2015-12-05"): return 11;
            case ("2015-12-08"): return 12;
        }

        return 0;
    }

    public void SetAllMissionState(string missionDate, bool value)
    {
        allMissions.missionButtons[MissionDateToButtonID(missionDate)].interactable = value;
    }

    public void SetFwdSonarState(string missionDate, bool value)
    {
        fwdSonarSelector.missionButtons[MissionDateToButtonID(missionDate)].interactable = value;
    }

    public void SetUpSonarState(string missionDate, bool value)
    {
        upSonarSelector.missionButtons[MissionDateToButtonID(missionDate)].interactable = value;
    }

    public void SetStateState(string missionDate, bool value)
    {
        stateSelector.missionButtons[MissionDateToButtonID(missionDate)].interactable = value;
    }

    public void SetCTDState(string missionDate, bool value)
    {
        ctdSelector.missionButtons[MissionDateToButtonID(missionDate)].interactable = value;
    }
}
