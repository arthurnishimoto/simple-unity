﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class MissionSelectUI : MonoBehaviour {

    public string mission;
    public Text missionButtonLabel;
    public Button missionButton;
    public Button sonarUpButton;
    public Button sonarFwdButton;
    public Button stateButton;
    public Button logButton;

    SIMPLEMissionManager missionManager;

    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
        if( missionButtonLabel )
            missionButtonLabel.text = mission;
    }

    public void SetMissionManager(SIMPLEMissionManager manager)
    {
        missionManager = manager;
    }

    public void OnMissionButtonClick()
    {
        missionManager.SetCurrentMission(mission);
    }

    public void OnSonarUpButtonClick()
    {
        missionManager.ToggleUpSonarDisplay();
    }

    public void OnSonarFwdButtonClick()
    {
        missionManager.ToggleFwdSonarDisplay();
    }

    public void OnStateButtonClick()
    {
        missionManager.ToggleDisplayState();
    }

    public void OnLogButtonClick()
    {

    }
}
