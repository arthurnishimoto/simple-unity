﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBarCanvasGUI : MonoBehaviour {

    Image progressBar;
    Text labelText;

    public string label;
    public float progress;

	// Use this for initialization
	void Start () {
        progressBar = transform.Find("Image").GetComponent<Image>();
        labelText = GetComponentInChildren<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        labelText.text = label;
        progressBar.transform.localScale = new Vector3(Mathf.Clamp(progress, 0, 1), 1, 1);

    }
}
