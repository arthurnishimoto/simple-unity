﻿using UnityEngine;
using UnityEngine.UI;

public class CanvasUIManager : MonoBehaviour {

    [Header("Input Sources")]
    [SerializeField]
    NavigatorGUI playerData;

    [SerializeField]
    NavigatorGUI auvData;

    [SerializeField]
    TimestampManager ctdData;

    [Header("Player Output UI")]
    [SerializeField]
    Text playerPosition;

    [SerializeField]
    Text playerCoordinates;

    [SerializeField]
    Text playerHeading;

    [Header("AUV Output UI")]
    [SerializeField]
    Text auvPosition;

    [SerializeField]
    Text auvCoordinates;

    [SerializeField]
    Text auvHeading;

    [SerializeField]
    Text timestamp;

    [SerializeField]
    Text ctdInfo;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        // Player info
	    if(playerPosition)
        {
            playerPosition.text = playerData.GetPosition();
        }
        if (playerCoordinates)
        {
            playerCoordinates.text = playerData.GetCoordinates();
        }
        if (playerHeading)
        {
            playerHeading.text = playerData.GetHeading();
        }

        // AUV info
        if (auvPosition)
        {
            auvPosition.text = auvData.GetPosition();
        }
        if (auvCoordinates)
        {
            auvCoordinates.text = auvData.GetCoordinates();
        }
        if (auvHeading)
        {
            auvHeading.text = auvData.GetHeading();
        }
        if (timestamp)
        {
            timestamp.text = auvData.GetTime();
        }
        if (ctdInfo)
        {
            ctdInfo.text = ctdData.GetCTDTextInfo();
        }
    }
}
