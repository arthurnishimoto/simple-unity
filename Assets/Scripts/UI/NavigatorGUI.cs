﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class NavigatorGUI : MonoBehaviour {
    public Transform cameraController;
    public TimestampManager timeScript;

    public SIMPLEDataLoader dataManager;

    // Use this for initialization
    void Start () {
        dataManager = GameObject.FindGameObjectWithTag("DataManager").GetComponent<SIMPLEDataLoader>();
    }

    public string GetPosition() {
        Vector3 pos = transform.position;
        return "Position: ( " + pos.x.ToString("F2") + " , " + pos.z.ToString("F2") + " ) Depth: " + pos.y.ToString("F2") + " meters rel. to melt hole";
    }
    
    public string GetCoordinates()
    {
        Vector3 pos = transform.position;
        float[] origin = utm.from_latlon(-77.89757f, 166.49123f);
        float[] coords = utm.to_latlon(pos.x + origin[0], pos.z + origin[1], 58, utm.ZONE_LETTERS.C, false);
        return "Coordinates: " + coords[0].ToString("F3") + " " + coords[1].ToString("F3");
    }

    public string GetHeading()
    {
        if (cameraController)
        {
            float heading = cameraController.eulerAngles.y;
            heading -= 180;
            //heading -= 270;
            //heading *= -1;
            if (heading < 0)
                heading += 360;

            string compassDirection = "";
            if (heading >= 360 - 22.5f || heading < 22.5)
            {
                compassDirection = "N";
            }
            else if (heading >= 22.5f && heading < 90 - 22.5)
            {
                compassDirection = "NE";
            }
            else if (heading >= 90 - 22.5f && heading < 90 + 22.5f)
            {
                compassDirection = "E";
            }
            else if (heading >= 90 + 22.5 && heading < 180 - 22.5f)
            {
                compassDirection = "SE";
            }
            else if (heading >= 180 - 22.5f && heading < 225 - 22.5f)
            {
                compassDirection = "S";
            }
            else if (heading >= 225 - 22.5f && heading < 270 - 22.5f)
            {
                compassDirection = "SW";
            }
            else if (heading >= 270 - 22.5f && heading < 270 + 22.5f)
            {
                compassDirection = "W";
            }
            else if (heading >= 270 + 22.5f && heading < 360 - 22.5f)
            {
                compassDirection = "NW";
            }

            return "Heading (Camera): " + heading.ToString("F2") + " degrees " + compassDirection;
        }
        return "N/A";
    }
    
    public string GetTime()
    {
        DateTime currentDateTime = timeScript.currentDateTime;

        string formattedDate = currentDateTime.Year.ToString() + "-";
        formattedDate += currentDateTime.Month.ToString() + "-";
        formattedDate += currentDateTime.Day.ToString();

        string formattedTime = "";
        if (currentDateTime.Hour < 10)
            formattedTime += "0" + currentDateTime.Hour.ToString() + ":";
        else
            formattedTime += "" + currentDateTime.Hour.ToString() + ":";
        if (currentDateTime.Minute < 10)
            formattedTime += "0" + currentDateTime.Minute.ToString() + ":";
        else
            formattedTime += "" + currentDateTime.Minute.ToString() + ":";
        if (currentDateTime.Second < 10)
            formattedTime += "0" + currentDateTime.Second.ToString() + ".";
        else
            formattedTime += "" + currentDateTime.Second.ToString() + ".";
        if (currentDateTime.Millisecond < 10)
            formattedTime += "00" + currentDateTime.Millisecond.ToString() + "";
        else if (currentDateTime.Millisecond < 100)
            formattedTime += "0" + currentDateTime.Millisecond.ToString() + "";
        else
            formattedTime += "" + currentDateTime.Millisecond.ToString() + "";

        return "Time: " + formattedDate + " " + formattedTime;
    }
}
