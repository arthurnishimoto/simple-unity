﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class RadarDataLoader : MonoBehaviour {

    [SerializeField]
    string dataPath = "/SIMPLE/radar";

    string displayNodeImagePath = "C:/ProgramData/Mechdyne/Cluster Tools Service/plugins_data";
    //string masterNodeImagePath = "C:/ProgramData/Mechdyne/Cluster Tools Service/plugins_data_client";
    string masterNodeImagePath = "C:/data";

    string[] dataFilePaths;

    ArrayList validFileList;

    [SerializeField]
    Material surfaceLineMaterial;

    [SerializeField]
    Material bedLineMaterial;

    [SerializeField]
    Material iceLineMaterial;

    // Use this for initialization
    void Start()
    {
        if (CAVE2.OnCAVE2Display())
        {
            dataPath = displayNodeImagePath + dataPath;
        }
        else
        {
            dataPath = masterNodeImagePath + dataPath;
        }

        dataFilePaths = Directory.GetFiles(dataPath);

        // Get Filenames
        validFileList = new ArrayList();
        foreach (string filePath in dataFilePaths)
        {
            // Testing subset
            //if (!filePath.Contains("2015-11-10"))
            //    continue;

            if (filePath.EndsWith(".meta") || filePath.EndsWith(".format"))
            {
                continue;
            }
            if (filePath.EndsWith(".txt"))
            {
                validFileList.Add(filePath);
            }
        }

        StartCoroutine("loadData", validFileList);
    }

    private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

    SortedDictionary<long, SIMPLEDataManager.SIMPLERadar> missionDataStates;

    IEnumerator loadData(ArrayList validFileList)
    {
        missionDataStates = new SortedDictionary<long, SIMPLEDataManager.SIMPLERadar>();

        foreach (string filePath in validFileList)
        {
            Debug.Log(this.GetType().Name + ": " + Path.GetFileName(filePath) + " found.");

            FileStream fs = File.Open(filePath, FileMode.Open);

            yield return new WaitWhile(() => fs.CanRead == false);

            FileInfo fileInfo = new FileInfo(filePath);
            string fileName = Path.GetFileNameWithoutExtension(filePath);

            StreamReader reader = new StreamReader(fs);

            ArrayList radarDataList = new ArrayList();
            while (!reader.EndOfStream)
            {
                string dataLine = reader.ReadLine();
                string[] dataArr = dataLine.Split(' ');

                int year = 0;
                float days = 0;
                float seconds = 0;

                if( !int.TryParse(dataArr[0], out year) )
                {
                    Debug.LogWarning("Bad year parse");
                }
                if (!float.TryParse(dataArr[1], out days))
                {
                    Debug.LogWarning("Bad day parse");
                }
                if (!float.TryParse(dataArr[2], out seconds))
                {
                    Debug.LogWarning("Bad seconds parse");
                }

                DateTime value = new DateTime();
                value = value.AddYears(year);
                value = value.AddDays(days);
                value = value.AddSeconds(seconds);

                TimeSpan elapsedTime = value - Epoch;
                long timestamp = (long)elapsedTime.TotalSeconds;

                float longitude = 0;
                float latitude = 0;
                if (!float.TryParse(dataArr[3], out longitude))
                {
                    //Debug.LogWarning("Bad longitude parse");
                    continue;
                }
                if (!float.TryParse(dataArr[4], out latitude))
                {
                    //Debug.LogWarning("Bad latitude parse");
                    continue;
                }

                float iceThickness = 0;
                float surfaceRange = 0;
                float bedElevation = 0;
                float surfaceElevation = 0;
                if (!float.TryParse(dataArr[5], out iceThickness))
                {

                }
                if (!float.TryParse(dataArr[6], out surfaceRange))
                {

                }
                if (!float.TryParse(dataArr[7], out bedElevation))
                {

                }
                if (!float.TryParse(dataArr[8], out surfaceElevation))
                {

                }

                SIMPLEDataManager.SIMPLERadar radarData;
                radarData.timestamp = timestamp;
                radarData.latLong = new Vector2(latitude, longitude);
                radarData.iceThickness = iceThickness;
                radarData.surfaceRange = surfaceRange;
                radarData.bedElevation = bedElevation;
                radarData.surfaceElevation = surfaceElevation;

                radarDataList.Add(radarData);
                missionDataStates[timestamp] = radarData;
                // Debug.Log(timestamp + " " + longitude + " " + latitude + " " + iceThickness + " " + surfaceRange + " " + bedElevation + " " + surfaceElevation);
            }

            // Create Line Renderer
            GameObject lineObject = new GameObject(fileName+"-surfaceElevation");
            lineObject.transform.parent = transform;
            lineObject.transform.localPosition = Vector3.zero;
            lineObject.transform.localRotation = Quaternion.identity;

            LineRenderer surfaceLine = lineObject.AddComponent<LineRenderer>();
            surfaceLine.positionCount = radarDataList.Count;
            surfaceLine.startWidth = 100;
            surfaceLine.endWidth = 100;
            surfaceLine.material = surfaceLineMaterial;
            surfaceLine.useWorldSpace = false;

            GameObject bedLineObject = new GameObject(fileName + "-bedElevation");
            bedLineObject.transform.parent = transform;
            bedLineObject.transform.localPosition = Vector3.zero;
            bedLineObject.transform.localRotation = Quaternion.identity;

            LineRenderer bedLine = bedLineObject.AddComponent<LineRenderer>();
            bedLine.positionCount = radarDataList.Count;
            bedLine.startWidth = 100;
            bedLine.endWidth = 100;
            bedLine.material = bedLineMaterial;
            bedLine.useWorldSpace = false;

            GameObject iceLineObject = new GameObject(fileName + "-iceElevation");
            iceLineObject.transform.parent = transform;
            iceLineObject.transform.localPosition = Vector3.zero;
            iceLineObject.transform.localRotation = Quaternion.identity;

            LineRenderer iceLine = iceLineObject.AddComponent<LineRenderer>();
            iceLine.positionCount = radarDataList.Count;
            iceLine.startWidth = 100;
            iceLine.endWidth = 100;
            iceLine.material = iceLineMaterial;
            iceLine.useWorldSpace = false;

            Vector3[] surfaceElevationPositions = new Vector3[radarDataList.Count];
            Vector3[] bedElevationPositions = new Vector3[radarDataList.Count];
            Vector3[] iceThicknessPositions = new Vector3[radarDataList.Count];

            float lastBedElevation = 0;
            float lastIceThickness = 0;
            for (int i = 0; i < radarDataList.Count; i++)
            {
                Vector3 latLong = ((SIMPLEDataManager.SIMPLERadar)radarDataList[i]).latLong;
                float surfaceElevation = ((SIMPLEDataManager.SIMPLERadar)radarDataList[i]).surfaceElevation;
                float bedElevation = ((SIMPLEDataManager.SIMPLERadar)radarDataList[i]).bedElevation;
                float iceThickness = ((SIMPLEDataManager.SIMPLERadar)radarDataList[i]).iceThickness;


                if (bedElevation == 0)
                {
                    bedElevation = lastBedElevation;
                }
                else
                {
                    lastBedElevation = bedElevation;  
                }

                if (iceThickness == 0)
                {
                    iceThickness = lastIceThickness;
                }
                else
                {
                    lastIceThickness = iceThickness;
                }

                surfaceElevationPositions[i] = LatLonToVector3(latLong.y, latLong.x, surfaceElevation);
                bedElevationPositions[i] = LatLonToVector3(latLong.y, latLong.x, bedElevation);
                iceThicknessPositions[i] = LatLonToVector3(latLong.y, latLong.x, surfaceElevation + -iceThickness);
            }
            surfaceLine.SetPositions(surfaceElevationPositions);
            bedLine.SetPositions(bedElevationPositions);
            iceLine.SetPositions(iceThicknessPositions);
        }

        Debug.Log("Loaded " + missionDataStates.Count + " radar states.");
        yield return null;
    }

    // Update is called once per frame
    void Update () {
		
	}

    Vector3 LatLonToVector3(float longitude, float latitude, float altitude = 0)
    {
        float[] coords = utm.from_latlon(latitude, longitude);
        float[] meltHole_coords = utm.from_latlon(-77.89757f, 166.49123f);
        coords[0] = meltHole_coords[0] - coords[0];
        coords[1] = meltHole_coords[1] - coords[1];
        return new Vector3(coords[0], altitude, coords[1]);
    }
}
