﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Navigator : MonoBehaviour {

    public float movementSpeed = 10;
    public float globalSpeedMod = 1;

    public new Transform camera;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 velocityVector = Vector3.zero;
		if( Input.GetKey(KeyCode.W) )
        {
            velocityVector += Vector3.forward * movementSpeed;
        }
        if (Input.GetKey(KeyCode.S))
        {
            velocityVector += Vector3.back * movementSpeed;
        }
        if (Input.GetKey(KeyCode.A))
        {
            velocityVector += Vector3.left * movementSpeed;
        }
        if (Input.GetKey(KeyCode.D))
        {
            velocityVector += Vector3.right * movementSpeed;
        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            velocityVector += Vector3.down * movementSpeed;
        }
        if (Input.GetKey(KeyCode.LeftControl))
        {
            velocityVector += Vector3.up * movementSpeed;
        }

        Vector3 pitchYawRoll = camera.eulerAngles;
        pitchYawRoll.x = 0;
        pitchYawRoll.z = 0;
        Quaternion rotation = Quaternion.Euler(pitchYawRoll);

        if( velocityVector.magnitude > 0 )
        {
            GetComponent<NavigatorGUI>().timeScript.setPositionBasedOnTime = false;
        }

        UpdateNavSpeed();
        transform.Translate( rotation * velocityVector * Time.deltaTime * globalSpeedMod);
    }

    public Text navLabel;
    public Slider navSlider;
    public void UpdateNavSpeed()
    {
        if (navLabel)
        {
            float sliderVal = navSlider.value;
            sliderVal = Mathf.Pow(10, (sliderVal - 5));


            navLabel.text = "Navigation Speed: " + sliderVal + "x";

            globalSpeedMod = sliderVal;
        }
    }
}
