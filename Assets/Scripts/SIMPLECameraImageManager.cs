﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections.Generic;
using System.Collections;

public class SIMPLECameraImageManager : MonoBehaviour {

    [SerializeField]
    TimestampManager timeManager;

    [SerializeField]
    long currentTimestamp = 1447404804577783;

    [SerializeField]
    float timeScale = 10;

    int currentIndex;
    int currentIndex2;
    int currentIndex3;

    SortedDictionary<long, string> spriteListCam1;
    List<string> spriteListValuesCam1;
    List<long> spriteListKeysCam1;

    SortedDictionary<long, string> spriteListCam2;
    List<string> spriteListValuesCam2;
    List<long> spriteListKeysCam2;

    SortedDictionary<long, string> spriteListCam3;
    List<string> spriteListValuesCam3;
    List<long> spriteListKeysCam3;

    [SerializeField]
    RawImage camera1;

    [SerializeField]
    RawImage camera2;

    [SerializeField]
    RawImage camera3;

    float timer;
    Texture2D texture1;
    Texture2D texture2;
    Texture2D texture3;

    string lastCam1File;
    string lastCam2File;
    string lastCam3File;

    [SerializeField]
    string imagePath = "/SIMPLE/images/lcmlog-";

    string displayNodeImagePath = "C:/ProgramData/Mechdyne/Cluster Tools Service/plugins_data";
    string masterNodeImagePath = "C:/ProgramData/Mechdyne/Cluster Tools Service/plugins_data_client";

    // Use this for initialization
    void Start () {
        if (CAVE2.OnCAVE2Display())
        {
            imagePath = displayNodeImagePath + imagePath;
        }
        else
        {
            imagePath = masterNodeImagePath + imagePath;
        }


        spriteListCam1 = new SortedDictionary<long, string>();
        spriteListCam2 = new SortedDictionary<long, string>();
        spriteListCam3 = new SortedDictionary<long, string>();

        string missionDate = timeManager.currentMission;

        if(missionDate == "2015-12-08")
        {
            missionDate += ".01";
        }
        else
        {
            missionDate += ".00";
        }

        // LoadFromPath(imagePath + missionDate + ".images/ART.CAMH1.CAM", spriteListCam1);
        // LoadFromPath(imagePath + missionDate + ".images/ART.CAMH2.CAM", spriteListCam2);

        StartCoroutine("LoadFromPath", imagePath);
    }
	
    IEnumerator LoadFromPath(string imagePath)
    {
        LoadFromPath(imagePath + "2015-11-13.00" + ".images/ART.CAMH1.CAM", spriteListCam1);
        LoadFromPath(imagePath + "2015-11-13.00" + ".images/ART.CAMH2.CAM", spriteListCam2);

        /* // Need smarter threaded way of loading these (too slow to startup)
        LoadFromPath(imagePath + "2015-11-15.00" + ".images/ART.CAMH1.CAM", spriteListCam1);
        LoadFromPath(imagePath + "2015-11-15.00" + ".images/ART.CAMH2.CAM", spriteListCam2);

        LoadFromPath(imagePath + "2015-11-17.00" + ".images/ART.CAMH1.CAM", spriteListCam1);
        LoadFromPath(imagePath + "2015-11-17.00" + ".images/ART.CAMH2.CAM", spriteListCam2);
        LoadFromPath(imagePath + "2015-11-17.00" + ".images/ART.CAMH3.CAM", spriteListCam3);

        LoadFromPath(imagePath + "2015-11-20.00" + ".images/ART.CAMH1.CAM", spriteListCam1);
        LoadFromPath(imagePath + "2015-11-20.00" + ".images/ART.CAMH2.CAM", spriteListCam2);
        LoadFromPath(imagePath + "2015-11-20.00" + ".images/ART.CAMH3.CAM", spriteListCam3);

        LoadFromPath(imagePath + "2015-11-23.00" + ".images/ART.CAMH1.CAM", spriteListCam1);
        LoadFromPath(imagePath + "2015-11-23.00" + ".images/ART.CAMH2.CAM", spriteListCam2);

        LoadFromPath(imagePath + "2015-11-25.00" + ".images/ART.CAMH1.CAM", spriteListCam1);
        LoadFromPath(imagePath + "2015-11-25.00" + ".images/ART.CAMH2.CAM", spriteListCam2);

        LoadFromPath(imagePath + "2015-11-27.00" + ".images/ART.CAMH1.CAM", spriteListCam1);
        LoadFromPath(imagePath + "2015-11-27.00" + ".images/ART.CAMH2.CAM", spriteListCam2);
        LoadFromPath(imagePath + "2015-11-27.00" + ".images/ART.CAMH3.CAM", spriteListCam3);

        LoadFromPath(imagePath + "2015-11-30.00" + ".images/ART.CAMH1.CAM", spriteListCam1);
        LoadFromPath(imagePath + "2015-11-30.00" + ".images/ART.CAMH2.CAM", spriteListCam2);
        LoadFromPath(imagePath + "2015-11-30.00" + ".images/ART.CAMH3.CAM", spriteListCam3);

        LoadFromPath(imagePath + "2015-12-03.00" + ".images/ART.CAMH1.CAM", spriteListCam1);
        LoadFromPath(imagePath + "2015-12-03.00" + ".images/ART.CAMH2.CAM", spriteListCam2);
        LoadFromPath(imagePath + "2015-12-03.00" + ".images/ART.CAMH3.CAM", spriteListCam3);
        */

        LoadFromPath(imagePath + "2015-12-05.00" + ".images/ART.CAMH1.CAM", spriteListCam1);
        LoadFromPath(imagePath + "2015-12-05.00" + ".images/ART.CAMH2.CAM", spriteListCam2);
        LoadFromPath(imagePath + "2015-12-05.00" + ".images/ART.CAMH3.CAM", spriteListCam3);

        /*
        LoadFromPath(imagePath + "2015-12-08.01" + ".images/ART.CAMH1.CAM", spriteListCam1);
        LoadFromPath(imagePath + "2015-12-08.01" + ".images/ART.CAMH2.CAM", spriteListCam2);
        LoadFromPath(imagePath + "2015-12-08.01" + ".images/ART.CAMH3.CAM", spriteListCam3);
        */

        spriteListKeysCam1 = new List<long>(spriteListCam1.Keys);
        spriteListKeysCam2 = new List<long>(spriteListCam2.Keys);
        spriteListKeysCam3 = new List<long>(spriteListCam3.Keys);

        spriteListValuesCam1 = new List<string>(spriteListCam1.Values);
        spriteListValuesCam2 = new List<string>(spriteListCam2.Values);
        spriteListValuesCam3 = new List<string>(spriteListCam3.Values);

        // Reuse the same texture for each camera to save memory
        texture1 = new Texture2D(2, 2);
        texture2 = new Texture2D(2, 2);
        texture3 = new Texture2D(2, 2);

        yield return null;
    }

	// Update is called once per frame
	void LateUpdate () {
        if (currentTimestamp != timeManager.currentTimestamp)
        {
            currentTimestamp = timeManager.currentTimestamp;
            try
            {
                // Check last file loaded to prevent redundant loading of images
                string newCam1File = spriteListValuesCam1[~spriteListKeysCam1.BinarySearch(currentTimestamp)];
                if (lastCam1File != newCam1File)
                {
                    //camera1.sprite = LoadSpriteFromFile(texture1, newCam1File);
                    StartCoroutine("LoadTexture1", newCam1File);

                    lastCam1File = newCam1File;
                }
                string newCam2File = spriteListValuesCam2[~spriteListKeysCam2.BinarySearch(currentTimestamp)];
                if (lastCam2File != newCam2File)
                {
                    //camera2.sprite = LoadSpriteFromFile(texture2, newCam2File);
                    StartCoroutine("LoadTexture2", newCam2File);

                    lastCam2File = newCam2File;
                    
                }
            }
            catch (System.Exception)
            {
                //Debug.LogError(e.Message);
            }

            //currentTimestamp += (long)(Time.deltaTime * 1000000 * timeScale);
            
        }
    }

    void LoadFromPath(string dataPath, SortedDictionary<long, string> table)
    {
        string[] dataFilePaths;
        try
        {
            dataFilePaths = Directory.GetFiles(dataPath);
        }
        catch (DirectoryNotFoundException)
        {
            //dataFilePaths = Directory.GetFiles(Application.dataPath + "/Resources/Data/");
            return;
        }
        Debug.Log("LoadFromPath: " + dataPath + " " + dataFilePaths.Length + " images");

        //byte[] fileData;
        //Texture2D texture;
        int imageLoadLimit = -1;
        int loadedImageCount = 0;
        foreach (string filePath in dataFilePaths)
        {
            if (File.Exists(filePath))
            {
                //fileData = File.ReadAllBytes(filePath);
                //texture = new Texture2D(2, 2);
                //texture.LoadImage(fileData);

                long fileNameTimestamp = 0;
                if (long.TryParse(Path.GetFileNameWithoutExtension(filePath), out fileNameTimestamp))
                {
                    // Debug.Log(fileNameTimestamp);
                    //Sprite sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
                    //sprite.name = fileNameTimestamp.ToString();

                    table[fileNameTimestamp] = filePath;
                    //GetComponent<Image>().sprite = (Sprite)spriteList[fileNameTimestamp];

                    loadedImageCount++;
                    if (imageLoadLimit > 0 && loadedImageCount >= imageLoadLimit)
                    {
                        break;
                    }
                }
            }
        }
    }

    Sprite LoadSpriteFromFile(Texture2D texture, string filePath)
    {
        if (File.Exists(filePath))
        {
            byte[] fileData = File.ReadAllBytes(filePath);
            texture.LoadImage(fileData);

            long fileNameTimestamp = 0;
            if (long.TryParse(Path.GetFileNameWithoutExtension(filePath), out fileNameTimestamp))
            {
                // Debug.Log(fileNameTimestamp);
                Sprite sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
                sprite.name = fileNameTimestamp.ToString();
                return sprite;
            }
        }
        return null;
    }

    IEnumerator LoadTexture1(string filePath)
    {
        if (File.Exists(filePath))
        {
            byte[] fileData = File.ReadAllBytes(filePath);
            texture1.LoadImage(fileData);
            camera1.texture = texture1;

            long fileNameTimestamp = 0;
            if (long.TryParse(Path.GetFileNameWithoutExtension(filePath), out fileNameTimestamp))
            {
                // Debug.Log(fileNameTimestamp);
                //Sprite sprite = Sprite.Create(texture1, new Rect(0.0f, 0.0f, texture1.width, texture1.height), new Vector2(0.5f, 0.5f), 100.0f);
                //sprite.name = fileNameTimestamp.ToString();
                //camera1.sprite = sprite;
            }
        }
        yield return null;
    }

    IEnumerator LoadTexture2(string filePath)
    {
        if (File.Exists(filePath))
        {
            byte[] fileData = File.ReadAllBytes(filePath);
            texture2.LoadImage(fileData);
            camera2.texture = texture2;

            long fileNameTimestamp = 0;
            if (long.TryParse(Path.GetFileNameWithoutExtension(filePath), out fileNameTimestamp))
            {
                // Debug.Log(fileNameTimestamp);
                //Sprite sprite = Sprite.Create(texture2, new Rect(0.0f, 0.0f, texture2.width, texture2.height), new Vector2(0.5f, 0.5f), 100.0f);
                //sprite.name = fileNameTimestamp.ToString();
                //camera2.sprite = sprite;
            }
        }
        yield return null;
    }
}
