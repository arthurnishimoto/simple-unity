﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SIMPLEPointCloudManager : MonoBehaviour {
    public enum PointCloudType { Sonar, State, CTD }

    // Point Cloud -------------------------------------------------------------------------------------------------------
    public bool cacheData = false;
    public Material matVertex;
    public Shader pointShader;
    public Shader cubeShader;

    public float pointSize = 0.2f;
    public float pointAlpha = 1.0f;
    private int limitPoints = 65000;

    public float ctdPointSize = 0.5f;
    public float ctdPointAlpha = 1.0f;

    public MeshRenderer[] sonarRenderers;
    public MeshRenderer[] ctdRenderers;

    public UnityEngine.UI.Text pointSizeText;
    public UnityEngine.UI.Text pointAlphaText;

    public UnityEngine.UI.Text ctdColorText;
    public UnityEngine.UI.Text ctdPointSizeText;
    public UnityEngine.UI.Text ctdPointAlphaText;

    public UnityEngine.UI.InputField pointSizeInputField;
    GameObject missionManager;

    public float[] conductivityBounds = { 0.0f, 2.8f };
    public float[] salinityBounds = { 0.0f, 35.0f };
    public float[] temperatureBounds = { -2.0f, 0.0f };
    public float[] pressureBounds = { 0.0f, 720000.0f };
    public float[] o2ConcentrationBounds = { 9.0f, 13.0f };
    public float[] o2PercentSaturationBounds = { 74.5f, 98.0f };

    private void Start()
    {
        if (pointSizeInputField)
            pointSizeInputField.text = pointSize.ToString();
        if (pointSizeText)
            pointSizeText.text = "Point Diameter: " + pointSize.ToString() + "m";

        SetSonarRenderShape(0);

        missionManager = GameObject.Find("MissionManager");
    }

    IEnumerator GeneratePointCloud(SIMPLEDataManager.PointCloudData data)
    {
        string dPath = data.filePath;
        Vector3[] points = data.positions;
        Color[] colors = data.colors;

        Debug.Log("GeneratePointCloud: " + data.filePath);

        int numPoints = data.nPoints;
        Vector3 minValue = new Vector3();

        // Instantiate Point Groups
        int numPointGroups = Mathf.CeilToInt(numPoints * 1.0f / limitPoints * 1.0f);

        //Debug.Log(limitPoints + " exceeded. Building " + numPointGroups + " point groups.");
        GameObject pointCloud = new GameObject(Path.GetFileName(dPath));
        pointCloud.transform.parent = GameObject.Find("World").transform;
        pointCloud.transform.localPosition = Vector3.zero;
        pointCloud.transform.localRotation = Quaternion.identity;
        pointCloud.transform.localScale = Vector3.one;

        PointCloudType type = data.type;

        for (int i = 0; i < numPointGroups - 1; i++)
        {
            InstantiateMesh(pointCloud, Path.GetFileName(dPath), i, limitPoints, points, colors, minValue, type);
            if (i % 10 == 0)
            {
                //guiText = i.ToString() + " out of " + numPointGroups.ToString() + " PointGroups loaded";
                yield return null;
            }
        }
        InstantiateMesh(pointCloud, Path.GetFileName(dPath), numPointGroups - 1, numPoints - (numPointGroups - 1) * limitPoints, points, colors, minValue, type);

        // Store PointCloud
        if (cacheData)
        {
#if UNITY_EDITOR
            UnityEditor.PrefabUtility.CreatePrefab("Assets/Resources/PointCloudMeshes/" + Path.GetFileName(dPath) + ".prefab", pointCloud);
#endif
        }
    }

    void InstantiateMesh(GameObject pointCloud, string filename, int meshInd, int nPoints, Vector3[] points, Color[] colors, Vector3 minValue, PointCloudType type )
    {
        // Create Mesh
        GameObject pointGroup = new GameObject(filename + meshInd);
        pointGroup.AddComponent<MeshFilter>();
        MeshRenderer renderer = pointGroup.AddComponent<MeshRenderer>();
        pointGroup.GetComponent<Renderer>().material = matVertex;

        pointGroup.GetComponent<MeshFilter>().mesh = CreateMesh(meshInd, nPoints, points, colors, minValue);
        pointGroup.transform.parent = pointCloud.transform;

        //pointGroup.transform.parent = GameObject.Find("World").transform;
        pointGroup.transform.localPosition = Vector3.zero;
        pointGroup.transform.localRotation = Quaternion.identity;
        pointGroup.transform.localScale = Vector3.one;

        renderer.enabled = false;

        if (type == PointCloudType.Sonar)
        {
            AddSonarRenderer(renderer);
        }
        else if(type == PointCloudType.CTD)
        {
            AddCTDRenderer(renderer);
        }

        // Store Mesh
        if (cacheData)
        {
#if UNITY_EDITOR
            UnityEditor.AssetDatabase.CreateAsset(pointGroup.GetComponent<MeshFilter>().mesh, "Assets/Resources/PointCloudMeshes/" + filename + @"/" + filename + meshInd + ".asset");
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
#endif
        }
    }

    Mesh CreateMesh(int id, int nPoints, Vector3[] points, Color[] colors, Vector3 minValue)
    {
        //Debug.Log(" Point Group: " + id + " " + nPoints + "/" + points.Length);
        Mesh mesh = new Mesh();

        Vector3[] myPoints = new Vector3[nPoints];
        int[] indecies = new int[nPoints];
        Color[] myColors = new Color[nPoints];

        for (int i = 0; i < nPoints; ++i)
        {
            myPoints[i] = points[id * limitPoints + i] - minValue;
            indecies[i] = i;
            myColors[i] = colors[id * limitPoints + i];
        }


        mesh.vertices = myPoints;
        mesh.colors = myColors;
        mesh.SetIndices(indecies, MeshTopology.Points, 0);
        mesh.uv = new Vector2[nPoints];
        mesh.normals = new Vector3[nPoints];


        return mesh;
    }

    public void AddSonarRenderer(MeshRenderer newRenderer)
    {
        if( GetComponent<TimestampManager>().sonarColorMode == TimestampManager.SonarColorMode.Material )
        {
            newRenderer.material.color = GetColor(-1);
        }
        else if (GetComponent<TimestampManager>().sonarColorMode == TimestampManager.SonarColorMode.Mission)
        {
            newRenderer.material.color = GetMissionColor(newRenderer.name);
        }

        ArrayList newList = new ArrayList();
        for (int i = 0; i < sonarRenderers.Length; i++)
        {
            newList.Add(sonarRenderers[i]);
        }
        newList.Add(newRenderer);

        sonarRenderers = new MeshRenderer[newList.Count];
        for (int i = 0; i < sonarRenderers.Length; i++)
        {
            sonarRenderers[i] = (MeshRenderer)newList[i];
        }
    }

    public void AddCTDRenderer(MeshRenderer newRenderer)
    {
        /*
        if (GetComponent<TimestampManager>().ctdColorMode == TimestampManager.cd)
        {
            newRenderer.material.color = GetColor(-1);
        }
        else if (GetComponent<TimestampManager>().sonarColorMode == TimestampManager.SonarColorMode.Mission)
        {
            newRenderer.material.color = GetMissionColor(newRenderer.name);
        }*/

        ArrayList newList = new ArrayList();
        for (int i = 0; i < ctdRenderers.Length; i++)
        {
            newList.Add(ctdRenderers[i]);
        }
        newList.Add(newRenderer);

        ctdRenderers = new MeshRenderer[newList.Count];
        for (int i = 0; i < ctdRenderers.Length; i++)
        {
            ctdRenderers[i] = (MeshRenderer)newList[i];
        }
    }

    public void SetSonarRenderShape(int value)
    {
        if (value == 1)
        {
            matVertex.shader = pointShader;
            if(pointSizeInputField)
                pointSizeInputField.interactable = false;
        }
        else if (value == 0)
        {
            matVertex.shader = cubeShader;
            if (pointSizeInputField)
                pointSizeInputField.interactable = true;
        }

        foreach (MeshRenderer rend in sonarRenderers)
        {
            rend.material.shader = matVertex.shader;
        }
    }

    float currentTimestamp;
    float missionStart;
    int sonarColorMode;
    bool colorByTimestamp;

    void Update()
    {
        foreach (MeshRenderer meshR in sonarRenderers)
        {
            meshR.material.SetFloat("_PointSize", pointSize);
            meshR.material.SetFloat("unif_FieldMax", currentTimestamp);
            meshR.material.SetFloat("unif_FieldMin", missionStart);
            meshR.material.SetFloat("unif_W1", 0.0f);
            meshR.material.SetFloat("unif_W2", 0.0f);
            meshR.material.SetFloat("unif_W3", sonarColorMode);
            meshR.material.SetFloat("unif_W4", colorByTimestamp ? 1.0f : 0.0f);

            meshR.material.SetColor("_Color", new Color(meshR.material.color.r, meshR.material.color.g, meshR.material.color.b, pointAlpha));
        }

        foreach (MeshRenderer meshR in ctdRenderers)
        {
            meshR.material.SetFloat("_PointSize", ctdPointSize);
            meshR.material.SetColor("_Color", new Color(meshR.material.color.r, meshR.material.color.g, meshR.material.color.b, ctdPointAlpha));
        }
    }

    public static Color GetMissionColor(string mission)
    {
        if (mission.Contains("2015-11-10"))
            return GetColor(0);
        else if (mission.Contains("2015-11-11"))
            return GetColor(1);
        else if (mission.Contains("2015-11-13"))
            return GetColor(2);
        else if (mission.Contains("2015-11-15"))
            return GetColor(3);
        else if (mission.Contains("2015-11-17"))
            return GetColor(4);
        else if (mission.Contains("2015-11-20"))
            return GetColor(5);
        else if (mission.Contains("2015-11-23"))
            return GetColor(6);
        else if (mission.Contains("2015-11-25"))
            return GetColor(7);
        else if (mission.Contains("2015-11-27"))
            return GetColor(8);
        else if (mission.Contains("2015-11-30"))
            return GetColor(9);
        else if (mission.Contains("2015-12-03"))
            return GetColor(10);
        else if (mission.Contains("2015-12-05"))
            return GetColor(11);
        else if (mission.Contains("2015-12-08"))
            return GetColor(12);
        else
            return GetColor(-1);
    }

    public void ColorByMission(bool value)
    {
        foreach (MeshRenderer meshR in sonarRenderers)
        {
            if (value)
            {
                if (meshR.name.Contains("2015-11-10"))
                    meshR.material.color = GetColor(0);
                else if (meshR.name.Contains("2015-11-11"))
                    meshR.material.color = GetColor(1);
                else if (meshR.name.Contains("2015-11-13"))
                    meshR.material.color = GetColor(2);
                else if (meshR.name.Contains("2015-11-15"))
                    meshR.material.color = GetColor(3);
                else if (meshR.name.Contains("2015-11-17"))
                    meshR.material.color = GetColor(4);
                else if (meshR.name.Contains("2015-11-20"))
                    meshR.material.color = GetColor(5);
                else if (meshR.name.Contains("2015-11-23"))
                    meshR.material.color = GetColor(6);
                else if (meshR.name.Contains("2015-11-25"))
                    meshR.material.color = GetColor(7);
                else if (meshR.name.Contains("2015-11-27"))
                    meshR.material.color = GetColor(8);
                else if (meshR.name.Contains("2015-11-30"))
                    meshR.material.color = GetColor(9);
                else if (meshR.name.Contains("2015-12-03"))
                    meshR.material.color = GetColor(10);
                else if (meshR.name.Contains("2015-12-05"))
                    meshR.material.color = GetColor(11);
                else if (meshR.name.Contains("2015-12-08"))
                    meshR.material.color = GetColor(12);
            }
            else
            {
                meshR.material.color = GetColor(-1);
            }
        }
    }
    static Color GetColor(int value)
    {
        Color output = Color.white;
        switch(value)
        {
            case (0): ColorUtility.TryParseHtmlString("#AFBD16", out output); break;
            case (1): ColorUtility.TryParseHtmlString("#FFBD16", out output); break;
            case (2): ColorUtility.TryParseHtmlString("#FF7F99", out output); break;
            case (3): ColorUtility.TryParseHtmlString("#C09BFF", out output); break;
            case (4): ColorUtility.TryParseHtmlString("#3FFFB5", out output); break;
            case (5): ColorUtility.TryParseHtmlString("#8CFF00", out output); break;
            case (6): ColorUtility.TryParseHtmlString("#AABF0D", out output); break;
            case (7): ColorUtility.TryParseHtmlString("#FF1111", out output); break;
            case (8): ColorUtility.TryParseHtmlString("#11FF11", out output); break;
            case (9): ColorUtility.TryParseHtmlString("#1111EE", out output); break;
            case (10): ColorUtility.TryParseHtmlString("#EEEE11", out output); break;
            case (11): ColorUtility.TryParseHtmlString("#EE11EE", out output); break;
            case (12): ColorUtility.TryParseHtmlString("#11EEEE", out output); break;
            default: ColorUtility.TryParseHtmlString("#05D2E5", out output); break;
        }
        return output;
    }

    public void SetSIMPLEShaderVars(float currentTimestamp, float missionStart, int sonarColorMode, bool colorByTimestamp)
    {
        this.currentTimestamp = currentTimestamp;
        this.missionStart = missionStart;
        this.sonarColorMode = sonarColorMode;
        this.colorByTimestamp = colorByTimestamp;
    }

    public void SetPointSize(string pointSize)
    {
        float newSize = float.Parse(pointSize);
        this.pointSize = newSize;
    }

    public void SetSonarPointSize(float newSize)
    {
        pointSize = newSize / 10.0f;

        if (pointSizeText)
            pointSizeText.text = "Point Width: " + pointSize.ToString() + "m";
    }

    public void SetTransparency(float value)
    {
        pointAlpha = (value / 10.0f);

        if (pointAlphaText)
            pointAlphaText.text = "Point Alpha: " + (pointAlpha).ToString("F1");
    }

    public void ColorCTDByConductivity()
    {
        foreach (MeshRenderer meshR in ctdRenderers)
        {
            meshR.material.SetFloat("_PointSize", ctdPointSize);
            meshR.material.SetFloat("unif_FieldMax", conductivityBounds[1]);
            meshR.material.SetFloat("unif_FieldMin", conductivityBounds[0]);
            meshR.material.SetFloat("unif_W1", 1.0f);
            meshR.material.SetFloat("unif_W2", 1.0f);
            meshR.material.SetFloat("unif_W3", sonarColorMode);
            meshR.material.SetFloat("unif_W4", 0.0f);
            meshR.material.color = new Color(meshR.material.color.r, meshR.material.color.g, meshR.material.color.b, ctdPointAlpha);
        }

        missionManager.BroadcastMessage("ShowCTD1");

        if (ctdColorText)
            ctdColorText.text = "Range: " + conductivityBounds[0] + " to " + conductivityBounds[1];
    }

    public void ColorCTDBySalinity()
    {
        foreach (MeshRenderer meshR in ctdRenderers)
        {
            meshR.material.SetFloat("_PointSize", ctdPointSize);
            meshR.material.SetFloat("unif_FieldMax", salinityBounds[1]);
            meshR.material.SetFloat("unif_FieldMin", salinityBounds[0]);
            meshR.material.SetFloat("unif_W1", 1.0f);
            meshR.material.SetFloat("unif_W2", 2.0f);
            meshR.material.SetFloat("unif_W3", sonarColorMode);
            meshR.material.SetFloat("unif_W4", colorByTimestamp ? 1.0f : 0.0f);
            meshR.material.color = new Color(meshR.material.color.r, meshR.material.color.g, meshR.material.color.b, ctdPointAlpha);
        }

        missionManager.BroadcastMessage("ShowCTD1");

        if (ctdColorText)
            ctdColorText.text = "Range: " + salinityBounds[0] + " to " + salinityBounds[1];
    }

    public void ColorCTDByTemperature()
    {
        /*
         * meshR.material.SetFloat("_PointSize", pointSize);
            meshR.material.SetFloat("unif_FieldMax", currentTimestamp);
            meshR.material.SetFloat("unif_FieldMin", missionStart);
            meshR.material.SetFloat("unif_W1", 0.0f);
            meshR.material.SetFloat("unif_W2", 0.0f);
            meshR.material.SetFloat("unif_W3", sonarColorMode);
            meshR.material.SetFloat("unif_W4", colorByTimestamp ? 1.0f : 0.0f);
         */
        foreach (MeshRenderer meshR in ctdRenderers)
        {
            meshR.material.SetFloat("_PointSize", ctdPointSize);
            meshR.material.SetFloat("unif_FieldMax", temperatureBounds[1]);
            meshR.material.SetFloat("unif_FieldMin", temperatureBounds[0]);
            meshR.material.SetFloat("unif_W1", 1.0f);
            meshR.material.SetFloat("unif_W2", 3.0f);
            meshR.material.SetFloat("unif_W3", sonarColorMode);
            meshR.material.SetFloat("unif_W4", colorByTimestamp ? 1.0f : 0.0f);
            meshR.material.color = new Color(meshR.material.color.r, meshR.material.color.g, meshR.material.color.b, ctdPointAlpha);
        }

        missionManager.BroadcastMessage("ShowCTD1");

        if (ctdColorText)
            ctdColorText.text = "Range: " + temperatureBounds[0] + " to " + temperatureBounds[1];
    }

    public void ColorCTDByPressure()
    {
        foreach (MeshRenderer meshR in ctdRenderers)
        {
            meshR.material.SetFloat("_PointSize", ctdPointSize);
            meshR.material.SetFloat("unif_FieldMax", pressureBounds[1]);
            meshR.material.SetFloat("unif_FieldMin", pressureBounds[0]);
            meshR.material.SetFloat("unif_W1", 1.0f);
            meshR.material.SetFloat("unif_W2", 1.0f);
            meshR.material.SetFloat("unif_W3", sonarColorMode);
            meshR.material.SetFloat("unif_W4", colorByTimestamp ? 1.0f : 0.0f);
            meshR.material.color = new Color(meshR.material.color.r, meshR.material.color.g, meshR.material.color.b, ctdPointAlpha);
        }

        missionManager.BroadcastMessage("ShowCTD2");

        if (ctdColorText)
            ctdColorText.text = "Range: " + pressureBounds[0] + " to " + pressureBounds[1];
    }

    public void ColorCTDByO2Concentration()
    {
        foreach (MeshRenderer meshR in ctdRenderers)
        {
            meshR.material.SetFloat("_PointSize", ctdPointSize);
            meshR.material.SetFloat("unif_FieldMax", o2ConcentrationBounds[1]);
            meshR.material.SetFloat("unif_FieldMin", o2ConcentrationBounds[0]);
            meshR.material.SetFloat("unif_W1", 1.0f);
            meshR.material.SetFloat("unif_W2", 2.0f);
            meshR.material.SetFloat("unif_W3", sonarColorMode);
            meshR.material.SetFloat("unif_W4", colorByTimestamp ? 1.0f : 0.0f);
            meshR.material.color = new Color(meshR.material.color.r, meshR.material.color.g, meshR.material.color.b, ctdPointAlpha);
        }

        missionManager.BroadcastMessage("ShowCTD2");

        if (ctdColorText)
            ctdColorText.text = "Range: " + o2ConcentrationBounds[0] + " to " + o2ConcentrationBounds[1];
    }

    public void ColorCTDByO2Saturation()
    {
        foreach (MeshRenderer meshR in ctdRenderers)
        {
            meshR.material.SetFloat("_PointSize", ctdPointSize);
            meshR.material.SetFloat("unif_FieldMax", o2PercentSaturationBounds[1]);
            meshR.material.SetFloat("unif_FieldMin", o2PercentSaturationBounds[0]);
            meshR.material.SetFloat("unif_W1", 1.0f);
            meshR.material.SetFloat("unif_W2", 2.0f);
            meshR.material.SetFloat("unif_W3", sonarColorMode);
            meshR.material.SetFloat("unif_W4", colorByTimestamp ? 1.0f : 0.0f);
            meshR.material.color = new Color(meshR.material.color.r, meshR.material.color.g, meshR.material.color.b, ctdPointAlpha);
        }

        missionManager.BroadcastMessage("ShowCTD2");

        if (ctdColorText)
            ctdColorText.text = "Range: " + o2PercentSaturationBounds[0] + " to " + o2PercentSaturationBounds[1];
    }
}
