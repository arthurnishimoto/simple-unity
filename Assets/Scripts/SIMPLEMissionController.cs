﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SIMPLEMissionController : MonoBehaviour
{

    public GameObject missionManager;
    public SIMPLEMissionManager[] missionManagers;

    public Toggle[] missionButtons;

    // Use this for initialization
    void Start()
    {
        missionManagers = missionManager.GetComponentsInChildren<SIMPLEMissionManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ToggleAllMissions(bool value)
    {
        if (value)
        {
            ShowAllMissions();
        }
        else
        {
            HideAllMissions();
        }
    }

    public void ShowAllMissions()
    {
        foreach (SIMPLEMissionManager m in missionManagers)
        {
            m.ShowAll();
        }
        foreach (Toggle b in missionButtons)
        {
            b.isOn = true;
        }
    }

    public void HideAllMissions()
    {
        foreach (SIMPLEMissionManager m in missionManagers)
        {
            m.HideAll();
        }
        foreach (Toggle b in missionButtons)
        {
            b.isOn = false;
        }
    }

    // Toggle Mission
    public void ToggleMission1(bool value)
    {
        if (value)
            missionManagers[0].ShowAll();
        else
            missionManagers[0].HideAll();
    }

    public void ToggleMission2(bool value)
    {
        if (value)
            missionManagers[1].ShowAll();
        else
            missionManagers[1].HideAll();
    }

    public void ToggleMission3(bool value)
    {
        if (value)
            missionManagers[2].ShowAll();
        else
            missionManagers[2].HideAll();
    }

    public void ToggleMission4(bool value)
    {
        if (value)
            missionManagers[3].ShowAll();
        else
            missionManagers[3].HideAll();
    }

    public void ToggleMission5(bool value)
    {
        if (value)
            missionManagers[4].ShowAll();
        else
            missionManagers[4].HideAll();
    }

    public void ToggleMission6(bool value)
    {
        if (value)
            missionManagers[5].ShowAll();
        else
            missionManagers[5].HideAll();
    }

    public void ToggleMission7(bool value)
    {
        if (value)
            missionManagers[6].ShowAll();
        else
            missionManagers[6].HideAll();
    }

    public void ToggleMission8(bool value)
    {
        if (value)
            missionManagers[7].ShowAll();
        else
            missionManagers[7].HideAll();
    }

    public void ToggleMission9(bool value)
    {
        if (value)
            missionManagers[8].ShowAll();
        else
            missionManagers[8].HideAll();
    }

    public void ToggleMission10(bool value)
    {
        if (value)
            missionManagers[9].ShowAll();
        else
            missionManagers[9].HideAll();
    }

    public void ToggleMission11(bool value)
    {
        if (value)
            missionManagers[10].ShowAll();
        else
            missionManagers[10].HideAll();
    }

    public void ToggleMission12(bool value)
    {
        if (value)
            missionManagers[11].ShowAll();
        else
            missionManagers[11].HideAll();
    }

    public void ToggleMission13(bool value)
    {
        if (value)
            missionManagers[12].ShowAll();
        else
            missionManagers[12].HideAll();
    }

    // Toggle Fwd Sonar
    public void ToggleAll_FwdSonar(bool value)
    {
        foreach (SIMPLEMissionManager m in missionManagers)
        {
            m.ToggleFwdSonar(value);
        }
        foreach (Toggle b in missionButtons)
        {
            b.isOn = value;
        }
    }

    public void ToggleMission1_FwdSonar(bool value)
    {
        missionManagers[0].ToggleFwdSonar(value);
    }

    public void ToggleMission2_FwdSonar(bool value)
    {
        missionManagers[1].ToggleFwdSonar(value);
    }

    public void ToggleMission3_FwdSonar(bool value)
    {
        missionManagers[2].ToggleFwdSonar(value);
    }

    public void ToggleMission4_FwdSonar(bool value)
    {
        missionManagers[3].ToggleFwdSonar(value);
    }

    public void ToggleMission5_FwdSonar(bool value)
    {
        missionManagers[4].ToggleFwdSonar(value);
    }

    public void ToggleMission6_FwdSonar(bool value)
    {
        missionManagers[5].ToggleFwdSonar(value);
    }

    public void ToggleMission7_FwdSonar(bool value)
    {
        missionManagers[6].ToggleFwdSonar(value);
    }

    public void ToggleMission8_FwdSonar(bool value)
    {
        missionManagers[7].ToggleFwdSonar(value);
    }

    public void ToggleMission9_FwdSonar(bool value)
    {
        missionManagers[8].ToggleFwdSonar(value);
    }

    public void ToggleMission10_FwdSonar(bool value)
    {
        missionManagers[9].ToggleFwdSonar(value);
    }

    public void ToggleMission11_FwdSonar(bool value)
    {
        missionManagers[10].ToggleFwdSonar(value);
    }

    public void ToggleMission12_FwdSonar(bool value)
    {
        missionManagers[11].ToggleFwdSonar(value);
    }

    public void ToggleMission13_FwdSonar(bool value)
    {
        missionManagers[12].ToggleFwdSonar(value);
    }

    // Toggle Up Sonar
    public void ToggleAll_UpSonar(bool value)
    {
        foreach (SIMPLEMissionManager m in missionManagers)
        {
            m.ToggleUpSonar(value);
        }
        foreach (Toggle b in missionButtons)
        {
            b.isOn = value;
        }
    }

    public void ToggleMission1_UpSonar(bool value)
    {
        missionManagers[0].ToggleUpSonar(value);
    }

    public void ToggleMission2_UpSonar(bool value)
    {
        missionManagers[1].ToggleUpSonar(value);
    }

    public void ToggleMission3_UpSonar(bool value)
    {
        missionManagers[2].ToggleUpSonar(value);
    }

    public void ToggleMission4_UpSonar(bool value)
    {
        missionManagers[3].ToggleUpSonar(value);
    }

    public void ToggleMission5_UpSonar(bool value)
    {
        missionManagers[4].ToggleUpSonar(value);
    }

    public void ToggleMission6_UpSonar(bool value)
    {
        missionManagers[5].ToggleUpSonar(value);
    }

    public void ToggleMission7_UpSonar(bool value)
    {
        missionManagers[6].ToggleUpSonar(value);
    }

    public void ToggleMission8_UpSonar(bool value)
    {
        missionManagers[7].ToggleUpSonar(value);
    }

    public void ToggleMission9_UpSonar(bool value)
    {
        missionManagers[8].ToggleUpSonar(value);
    }

    public void ToggleMission10_UpSonar(bool value)
    {
        missionManagers[9].ToggleUpSonar(value);
    }

    public void ToggleMission11_UpSonar(bool value)
    {
        missionManagers[10].ToggleUpSonar(value);
    }

    public void ToggleMission12_UpSonar(bool value)
    {
        missionManagers[11].ToggleUpSonar(value);
    }

    public void ToggleMission13_UpSonar(bool value)
    {
        missionManagers[12].ToggleUpSonar(value);
    }

    // Toggle State
    public void ToggleAll_State(bool value)
    {
        foreach (SIMPLEMissionManager m in missionManagers)
        {
            m.ToggleState(value);
        }
        foreach (Toggle b in missionButtons)
        {
            b.isOn = value;
        }
    }

    public void ToggleMission1_State(bool value)
    {
        missionManagers[0].ToggleState(value);
    }

    public void ToggleMission2_State(bool value)
    {
        missionManagers[1].ToggleState(value);
    }

    public void ToggleMission3_State(bool value)
    {
        missionManagers[2].ToggleState(value);
    }

    public void ToggleMission4_State(bool value)
    {
        missionManagers[3].ToggleState(value);
    }

    public void ToggleMission5_State(bool value)
    {
        missionManagers[4].ToggleState(value);
    }

    public void ToggleMission6_State(bool value)
    {
        missionManagers[5].ToggleState(value);
    }

    public void ToggleMission7_State(bool value)
    {
        missionManagers[6].ToggleState(value);
    }

    public void ToggleMission8_State(bool value)
    {
        missionManagers[7].ToggleState(value);
    }

    public void ToggleMission9_State(bool value)
    {
        missionManagers[8].ToggleState(value);
    }

    public void ToggleMission10_State(bool value)
    {
        missionManagers[9].ToggleState(value);
    }

    public void ToggleMission11_State(bool value)
    {
        missionManagers[10].ToggleState(value);
    }

    public void ToggleMission12_State(bool value)
    {
        missionManagers[11].ToggleState(value);
    }

    public void ToggleMission13_State(bool value)
    {
        missionManagers[12].ToggleState(value);
    }

    // Toggle CTD
    public void ToggleAll_CTD(bool value)
    {
        foreach (SIMPLEMissionManager m in missionManagers)
        {
            m.ToggleCTD(value);
        }
        foreach (Toggle b in missionButtons)
        {
            b.isOn = value;
        }
    }

    public void ToggleMission1_CTD(bool value)
    {
        missionManagers[0].ToggleCTD(value);
    }

    public void ToggleMission2_CTD(bool value)
    {
        missionManagers[1].ToggleCTD(value);
    }

    public void ToggleMission3_CTD(bool value)
    {
        missionManagers[2].ToggleCTD(value);
    }

    public void ToggleMission4_CTD(bool value)
    {
        missionManagers[3].ToggleCTD(value);
    }

    public void ToggleMission5_CTD(bool value)
    {
        missionManagers[4].ToggleCTD(value);
    }

    public void ToggleMission6_CTD(bool value)
    {
        missionManagers[5].ToggleCTD(value);
    }

    public void ToggleMission7_CTD(bool value)
    {
        missionManagers[6].ToggleCTD(value);
    }

    public void ToggleMission8_CTD(bool value)
    {
        missionManagers[7].ToggleCTD(value);
    }

    public void ToggleMission9_CTD(bool value)
    {
        missionManagers[8].ToggleCTD(value);
    }

    public void ToggleMission10_CTD(bool value)
    {
        missionManagers[9].ToggleCTD(value);
    }

    public void ToggleMission11_CTD(bool value)
    {
        missionManagers[10].ToggleCTD(value);
    }

    public void ToggleMission12_CTD(bool value)
    {
        missionManagers[11].ToggleCTD(value);
    }

    public void ToggleMission13_CTD(bool value)
    {
        missionManagers[12].ToggleCTD(value);
    }
}