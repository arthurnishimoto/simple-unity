﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class SIMPLEMarker : MonoBehaviour {

    public enum SIMPLE_SITE { None, McMurdo, SIMPLE_Camp, SIMPLE_Site_B, SIMPLE_Site_C, SIMPLE_Site_D, SIMPLE_Site_E, SIMPLE_Site_F, SIMPLE_MeltHole_2015 };

    public SIMPLE_SITE quickCoords;

    public float latitude, longitude;
    public utm.ZONE_LETTERS zoneLetter = utm.ZONE_LETTERS.None;
    public int zoneNumber;

    public bool zeroOnMeltHole = true;

    public bool setPosition = true;

    // Use this for initialization
    void Start () {
        switch (quickCoords)
        {
            case (SIMPLE_SITE.McMurdo): latitude = -77.84187f; longitude = 166.68634f; break;
            case (SIMPLE_SITE.SIMPLE_Camp): latitude = -77.86658f; longitude = 166.48763f; break;
            case (SIMPLE_SITE.SIMPLE_Site_B): latitude = -77.90133f; longitude = 167.27654f; break;
            case (SIMPLE_SITE.SIMPLE_Site_C): latitude = -77.99152f; longitude = 167.2109f; break;
            case (SIMPLE_SITE.SIMPLE_Site_D): latitude = -78.01175f; longitude = 166.97568f; break;
            case (SIMPLE_SITE.SIMPLE_Site_E): latitude = -78.09792f; longitude = 166.60053f; break;
            case (SIMPLE_SITE.SIMPLE_Site_F): latitude = -77.97422f; longitude = 166.48209f; break;
            case (SIMPLE_SITE.SIMPLE_MeltHole_2015): latitude = -77.89757f; longitude = 166.49123f; break;
        }

        float[] coords = utm.from_latlon(latitude, longitude);
        zoneLetter = utm.latitude_to_zone_letter(latitude);
        zoneNumber = utm.latlon_to_zone_number(latitude, longitude);

        if ( zeroOnMeltHole )
        {
            float[] meltHole_coords = utm.from_latlon(-77.89757f, 166.49123f);
            coords[0] = meltHole_coords[0] - coords[0];
            coords[1] = meltHole_coords[1] - coords[1];
        }
        if(setPosition)
            transform.localPosition = new Vector3(coords[0], 0, coords[1]);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void LateUpdate()
    {
        Transform markerText = transform.Find("Marker Text");
        if( markerText != null )
        {
            markerText.LookAt(GameObject.FindGameObjectWithTag("MainCamera").transform);
            markerText.Rotate(0, 180, 0);
        }
    }
}
