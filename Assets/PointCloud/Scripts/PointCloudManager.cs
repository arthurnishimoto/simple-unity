﻿using UnityEngine;
using System.Collections;
using System.IO;

public class PointCloudManager : MonoBehaviour {

	// File
	public string dataPath;
	//private string filename;
	public Material matVertex;

	// GUI
	private float progress = 0;
	private string guiText;
	private bool loaded = false;

	// PointCloud
	//private GameObject pointCloud;

	public float scale = 1;
	public bool invertYZ = false;
	public bool forceReload = false;

	//public int numPoints;
	//public int numPointGroups;
	private int limitPoints = 65000;

	//private Vector3[] points;
	//private Color[] colors;
	//private Vector3 minValue;

    string[] dataFilePaths;

    public bool loadFwdFacingSonar = true;
    public bool loadUpperFacingSonar = false;
    public bool loadState = false;

    MeshRenderer[] meshRenderers;

    public TimestampManager timeManager;

    void Start()
    {
        dataFilePaths = Directory.GetFiles(dataPath);


        // Create Resources folder
        createFolders();

        // Get Filenames
        foreach (string fileName in dataFilePaths)
        {
            if (fileName.EndsWith(".meta"))
            {
                continue;
            }
            if (!loadFwdFacingSonar && fileName.EndsWith("Fwd.xyzb"))
            {
                continue;
            }
            if (!loadUpperFacingSonar && fileName.EndsWith("Up.xyzb"))
            {
                continue;
            }
            if (!loadState && fileName.EndsWith("state.xyzb"))
            {
                continue;
            }
            // Debug.Log(Path.GetFileName(fileName));
            loaded = false;
            loadFiles(fileName);
        }

        meshRenderers = GetComponentsInChildren<MeshRenderer>();
        Debug.Log(meshRenderers.Length + " meshRenderers saved");
    }



	void loadFiles(string filepath)
    {
        // Check if the PointCloud was loaded previously
        if (!Directory.Exists(Application.dataPath + "/Resources/PointCloudMeshes/" + Path.GetFileName(filepath)))
        {
#if UNITY_EDITOR
            UnityEditor.AssetDatabase.CreateFolder("Assets/Resources/PointCloudMeshes", Path.GetFileName(filepath));
#endif
            loadPointCloud(filepath);
        }
        else if (forceReload)
        {
#if UNITY_EDITOR
            UnityEditor.FileUtil.DeleteFileOrDirectory(Application.dataPath + "/Resources/PointCloudMeshes/" + Path.GetFileName(filepath));
            UnityEditor.AssetDatabase.Refresh();
            UnityEditor.AssetDatabase.CreateFolder("Assets/Resources/PointCloudMeshes", Path.GetFileName(filepath));
#endif
            loadPointCloud(filepath);
        }
        else
        {
            // Load stored PointCloud
            loadStoredMeshes(Path.GetFileName(filepath));
        }
	}
	
	
	void loadPointCloud(string dataPath){
		// Check what file exists
        if (File.Exists(dataPath))
            StartCoroutine("loadXYZB", dataPath);
        else 
			Debug.Log ("File '" + dataPath + "' could not be found"); 
		
	}
	
	// Load stored PointCloud
	void loadStoredMeshes(string filename){
        Object res = Resources.Load("PointCloudMeshes/" + filename);
        if (res != null)
        {
            GameObject pointGroup = Instantiate(res) as GameObject;
            pointGroup.transform.parent = transform;
            pointGroup.transform.localPosition = Vector3.zero;
            pointGroup.transform.localRotation = Quaternion.identity;
            pointGroup.transform.localScale = Vector3.one;
            loaded = true;
        }
	}

    // Start Coroutine of reading the points from the XYZB file and creating the meshes
	/*
    IEnumerator loadXYZB(string dPath)
    {
        BinaryReader reader = new BinaryReader(File.Open(dPath, FileMode.Open));
        int numPoints = 0;
        ArrayList pointsAL = new ArrayList();
        ArrayList colorsAL = new ArrayList();

        while (true)
        {
            try
            {
                double x = reader.ReadDouble();
                double y = reader.ReadDouble();
                double z = reader.ReadDouble();

                double r = reader.ReadDouble();
                double g = reader.ReadDouble();
                double b = reader.ReadDouble();
                double a = reader.ReadDouble();

                pointsAL.Add(new Vector3((float)x, (float)y, (float)z));
                colorsAL.Add(new Color((float)r, (float)g, (float)b, (float)a));

                numPoints++;
            }
            catch (EndOfStreamException e)
            {
                //Debug.Log("End of file");
                reader.Close();
                break;
            }
        }

        Debug.Log("Loaded " + numPoints + " points from "+ Path.GetFileName(dPath));
        Vector3[] points = (Vector3[]) pointsAL.ToArray(typeof (Vector3));
        Color[] colors = (Color[]) colorsAL.ToArray(typeof(Color));
        Vector3 minValue = new Vector3();

        // Instantiate Point Groups
        int numPointGroups = Mathf.CeilToInt(numPoints * 1.0f / limitPoints * 1.0f);
        Debug.Log(limitPoints + " exceeded. Building " + numPointGroups + " point groups.");
        GameObject pointCloud = new GameObject(Path.GetFileName(dPath));

        for (int i = 0; i < numPointGroups - 1; i++)
        {
            InstantiateMesh(pointCloud, Path.GetFileName(dPath), i, limitPoints, points, colors, minValue);
            if (i % 10 == 0)
            {
                guiText = i.ToString() + " out of " + numPointGroups.ToString() + " PointGroups loaded";
                yield return null;
            }
        }
        InstantiateMesh(pointCloud, Path.GetFileName(dPath), numPointGroups - 1, numPoints - (numPointGroups - 1) * limitPoints, points, colors, minValue);

        //Store PointCloud
#if UNITY_EDITOR
        UnityEditor.PrefabUtility.CreatePrefab("Assets/Resources/PointCloudMeshes/" + Path.GetFileName(dPath) + ".prefab", pointCloud);
#endif
        loaded = true;
    }
	*/
	void InstantiateMesh(GameObject pointCloud, string filename, int meshInd, int nPoints, Vector3[] points, Color[] colors, Vector3 minValue){
		// Create Mesh
		GameObject pointGroup = new GameObject (filename + meshInd);
		pointGroup.AddComponent<MeshFilter> ();
		pointGroup.AddComponent<MeshRenderer> ();
		pointGroup.GetComponent<Renderer>().material = matVertex;

		pointGroup.GetComponent<MeshFilter> ().mesh = CreateMesh (meshInd, nPoints, points, colors, minValue);
		pointGroup.transform.parent = pointCloud.transform;

        pointGroup.transform.parent = transform;
        pointGroup.transform.localPosition = Vector3.zero;
        pointGroup.transform.localRotation = Quaternion.identity;
        pointGroup.transform.localScale = Vector3.one;

        // Store Mesh
#if UNITY_EDITOR
        UnityEditor.AssetDatabase.CreateAsset(pointGroup.GetComponent<MeshFilter> ().mesh, "Assets/Resources/PointCloudMeshes/" + filename + @"/" + filename + meshInd + ".asset");
		UnityEditor.AssetDatabase.SaveAssets ();
		UnityEditor.AssetDatabase.Refresh();
#endif
    }

	Mesh CreateMesh(int id, int nPoints, Vector3[] points, Color[] colors, Vector3 minValue)
    {
        Debug.Log(" Point Group: " + id + " " + nPoints + "/" + points.Length);
        Mesh mesh = new Mesh ();
		
		Vector3[] myPoints = new Vector3[nPoints]; 
		int[] indecies = new int[nPoints];
		Color[] myColors = new Color[nPoints];

		for(int i=0;i<nPoints;++i) {
			myPoints[i] = points[id*limitPoints + i] - minValue;
			indecies[i] = i;
			myColors[i] = colors[id*limitPoints + i];
		}


		mesh.vertices = myPoints;
		mesh.colors = myColors;
		mesh.SetIndices(indecies, MeshTopology.Points,0);
		mesh.uv = new Vector2[nPoints];
		mesh.normals = new Vector3[nPoints];


		return mesh;
	}

	Vector3 calculateMin(Vector3 point){
        Vector3 minValue = Vector3.zero;
		if (minValue.magnitude == 0)
			minValue = point;


		if (point.x < minValue.x)
			minValue.x = point.x;
		if (point.y < minValue.y)
			minValue.y = point.y;
		if (point.z < minValue.z)
			minValue.z = point.z;

        return minValue;
    }

	void createFolders(){
#if UNITY_EDITOR
        if (!Directory.Exists (Application.dataPath + "/Resources/"))
			UnityEditor.AssetDatabase.CreateFolder ("Assets", "Resources");

		if (!Directory.Exists (Application.dataPath + "/Resources/PointCloudMeshes/"))
			UnityEditor.AssetDatabase.CreateFolder ("Assets/Resources", "PointCloudMeshes");
#endif
    }


	void OnGUI(){


		if (!loaded){
			GUI.BeginGroup (new Rect(Screen.width/2-100, Screen.height/2, 400.0f, 20));
			GUI.Box (new Rect (0, 0, 200.0f, 20.0f), guiText);
			GUI.Box (new Rect (0, 0, progress*200.0f, 20), "");
			GUI.EndGroup ();
		}
	}

}
